package com.appetiser.module.network.features.track

import com.appetiser.module.domain.models.track.Track
import com.appetiser.module.domain.utils.any
import com.appetiser.module.network.ApiServiceModule
import com.appetiser.module.network.BaseplateApiServices
import com.appetiser.module.network.NetworkModule
import com.appetiser.module.network.features.Stubs
import com.appetiser.module.network.features.auth.models.response.TracksResponse
import com.appetiser.module.network.features.track.models.TrackDTO
import com.appetiser.module.network.features.user.UserRemoteSource
import com.appetiser.module.network.features.user.UserRemoteSourceImpl
import com.appetiser.module.network.features.user.models.UserDTO
import com.google.gson.Gson
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner
import javax.inject.Inject

@RunWith(MockitoJUnitRunner::class)
class TrackRemoteSourceImplTest {


    @Test
    fun getAllTracks_ShouldReturnList() {
        val networkModule = NetworkModule()
        val apiServiceModule = ApiServiceModule()
        val customInterceptor = networkModule.providesCustomHeaderInterceptor()
        val okHttpClient = networkModule.providesOkHttpClient(customInterceptor)
        val retrofit = networkModule.providesRetrofit(okHttpClient, Gson())
        val trackRemoteSource = networkModule.providesTrackRemoteSource(apiServiceModule.providesBaseplateApiServices(retrofit))

        val tracksApiResponse = trackRemoteSource.getTracks()
            .blockingGet()

        tracksApiResponse.map { track ->
            print("${track.collectionCensoredName} | ${track.artistName} | ${track.kind} | ${track.copyright}\n ")
        }

        tracksApiResponse
            .distinctBy { it.kind }
            .map { track ->
                print("${track.kind}\n")
            }

        assert(tracksApiResponse.isNotEmpty())

    }
}
