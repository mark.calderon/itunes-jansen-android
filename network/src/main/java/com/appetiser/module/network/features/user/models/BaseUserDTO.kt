package com.appetiser.module.network.features.user.models

import com.google.gson.annotations.SerializedName

open class BaseUserDTO(
    @field:SerializedName("last_name")
    open val lastName: String? = "",
    @field:SerializedName("first_name")
    open val firstName: String? = "",
    @field:SerializedName("full_name")
    open val fullName: String? = "",
    @field:SerializedName("avatar_permanent_url")
    open val avatarPermanentUrl: String? = "",
    @field:SerializedName("avatar_permanent_thumb_url")
    open val avatarPermanentThumbUrl: String? = "",
    open val id: String = ""
)
