package com.appetiser.module.network.features.user

import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.User
import io.reactivex.Completable
import io.reactivex.Single

interface UserRemoteSource {
    fun updateUser(
        accessToken: AccessToken,
        user: User
    ): Single<User>

    fun uploadPhoto(
        accessToken: AccessToken,
        filePath: String
    ): Completable
}
