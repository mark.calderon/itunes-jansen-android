package com.appetiser.module.network.features.track.models

import com.appetiser.module.domain.models.track.Track

data class TrackDTO(
    val wrapperType: String,
    val kind: String? = null,
    val artistId: Int,
    val collectionId: Int,
    val artistName: String,
    val trackName: String? = null,
    val collectionName: String? = null,
    val collectionCensoredName: String? = null,
    val artistViewUrl: String? = null,
    val collectionViewUrl: String? = null,
    val artworkUrl60: String,
    val artworkUrl100: String,
    val collectionPrice: Double,
    val collectionExplicitness: String,
    val trackCount: Int,
    val copyright: String? = null,
    val country: String,
    val currency: String,
    val releaseDate: String,
    val primaryGenreName: String,
    val previewUrl: String,
    val description: String? = null,
    val longDescription: String? = null
) {
    companion object {
        fun toDomain(track: TrackDTO): Track {
            return with(track) {
                Track(
                    collectionId = collectionId,
                    wrapperType = wrapperType,
                    kind = kind,
                    artistId = artistId,
                    artistName = artistName,
                    trackName = trackName,
                    collectionName = collectionName,
                    collectionCensoredName = collectionCensoredName,
                    artistViewUrl = artistViewUrl,
                    artworkUrl60 = artworkUrl60,
                    artworkUrl100 = artworkUrl100,
                    collectionPrice = collectionPrice,
                    collectionViewUrl = collectionViewUrl,
                    collectionExplicitness = collectionExplicitness,
                    trackCount = trackCount,
                    copyright = copyright,
                    country = country,
                    currency = currency,
                    releaseDate = releaseDate,
                    primaryGenreName = primaryGenreName,
                    previewUrl = previewUrl,
                    description = description,
                    longDescription = longDescription
                )
            }
        }
    }
}
