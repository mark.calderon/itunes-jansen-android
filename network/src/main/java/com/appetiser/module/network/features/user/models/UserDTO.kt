package com.appetiser.module.network.features.user.models

import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.User
import com.appetiser.module.network.features.auth.models.response.AuthDataResponse
import com.google.gson.annotations.SerializedName

class UserDTO(
    @field:SerializedName("phone_number")
    val phoneNumber: String? = "",
    @field:SerializedName("email_verified")
    val emailVerified: Boolean = false,
    @field:SerializedName("phone_number_verified")
    val phoneNumberVerified: Boolean = false,
    val verified: Boolean = false,
    val email: String? = ""
) : BaseUserDTO() {
    companion object {
        fun toDomain(user: UserDTO): User {
            return with(user) {
                User(
                    phoneNumber = phoneNumber.orEmpty(),
                    emailVerified = emailVerified,
                    phoneNumberVerified = phoneNumberVerified,
                    verified = verified,
                    email = email.orEmpty(),
                    lastName = lastName.orEmpty(),
                    firstName = firstName.orEmpty(),
                    fullName = fullName.orEmpty(),
                    avatarPermanentUrl = avatarPermanentUrl.orEmpty(),
                    avatarPermanentThumbUrl = avatarPermanentThumbUrl.orEmpty(),
                    id = id
                )
            }
        }

        fun mapAuthDataResponse(from: AuthDataResponse): Pair<User, AccessToken> {
            val user = from.data.user
            val userData = from.data

            return Pair(
                User(
                    phoneNumber = user.phoneNumber.orEmpty(),
                    emailVerified = user.emailVerified,
                    phoneNumberVerified = user.phoneNumberVerified,
                    verified = user.verified,
                    email = user.email.orEmpty(),
                    lastName = user.lastName.orEmpty(),
                    firstName = user.firstName.orEmpty(),
                    fullName = user.fullName.orEmpty(),
                    avatarPermanentUrl = user.avatarPermanentUrl.orEmpty(),
                    avatarPermanentThumbUrl = user.avatarPermanentThumbUrl.orEmpty(),
                    id = user.id
                ),
                AccessToken(
                    token = userData.accessToken,
                    refresh = "",
                    tokenType = userData.tokenType.orEmpty(),
                    expiresIn = userData.expiresIn.orEmpty()
                )
            )
        }
    }
}
