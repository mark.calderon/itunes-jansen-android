package com.appetiser.module.network.features.auth.models.response

import com.appetiser.module.network.features.track.models.TrackDTO

data class TracksResponse(val results: List<TrackDTO>)
