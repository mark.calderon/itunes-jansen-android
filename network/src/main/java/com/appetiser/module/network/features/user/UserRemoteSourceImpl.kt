package com.appetiser.module.network.features.user

import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.User
import com.appetiser.module.network.base.BaseRemoteSource
import com.appetiser.module.network.BaseplateApiServices
import com.appetiser.module.network.features.user.models.UserDTO
import io.reactivex.Completable
import io.reactivex.Single
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import javax.inject.Inject

class UserRemoteSourceImpl @Inject constructor(
    private val baseplateApiServices: BaseplateApiServices
) : BaseRemoteSource(), UserRemoteSource {
    override fun updateUser(accessToken: AccessToken, user: User): Single<User> {
        val json = user.toJsonStringExcludeEmpty()
        return baseplateApiServices
            .updateUserInfo(
                accessToken.bearerToken,
                getJsonRequestBody(json)
            )
            .map { it.data }
            .map { UserDTO.toDomain(it) }
    }

    override fun uploadPhoto(
        accessToken: AccessToken,
        filePath: String
    ): Completable {
        val imageFile = File(filePath)
        val requestImageBody = imageFile.asRequestBody("image/*".toMediaTypeOrNull())
        val requestBody =
            MultipartBody.Part.createFormData(
                "avatar",
                "profile_${imageFile.name}",
                requestImageBody
            )

        return baseplateApiServices
            .uploadPhoto(
                accessToken.bearerToken,
                requestBody
            )
    }
}
