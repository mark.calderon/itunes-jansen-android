package com.appetiser.module.network.features.auth

import com.appetiser.module.domain.models.auth.CountryCode
import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.User
import io.reactivex.Completable
import io.reactivex.Single

interface AuthRemoteSource {
    fun logout(accessToken: AccessToken): Completable

    fun forgotPassword(email: String): Single<Boolean>

    fun forgotPasswordCheckCode(email: String, code: String): Single<Boolean>

    fun newPassword(email: String, token: String, password: String, confirmPassword: String): Single<Boolean>

    /**
     * Checks if username has an existing account in backend.
     *
     * @param username email or phone number
     */
    fun checkUsername(username: String): Single<Boolean>

    fun login(username: String, password: String): Single<Pair<User, AccessToken>>

    /**
     * Call this method to login via social media.
     *
     * @param accessToken
     * @param accessTokenProvider provider of access token ["facebook", "google"].
     */
    fun socialLogin(accessToken: String, accessTokenProvider: String): Single<Pair<User, AccessToken>>

    /**
     * If user uses phone number for logging in. Pass only [phone].
     */
    fun register(
        email: String,
        phone: String,
        password: String,
        confirmPassword: String,
        firstName: String,
        lastName: String
    ): Single<Pair<User, AccessToken>>

    /**
     * @return Returns pair of boolean true/false if verification succeeds and user session.
     */
    fun verifyAccountEmail(accessToken: AccessToken, code: String): Single<Pair<Boolean, User>>

    fun verifyAccountMobilePhone(accessToken: AccessToken, code: String): Single<Pair<Boolean, User>>

    fun resendEmailVerificationCode(accessToken: AccessToken): Single<Boolean>

    fun resendMobilePhoneVerificationCode(accessToken: AccessToken): Single<Boolean>

    fun getCountryCode(countryCodes: List<Int>): Single<List<CountryCode>>
}
