package com.appetiser.module.network.features.track

import com.appetiser.module.domain.models.track.Track
import com.appetiser.module.network.BaseplateApiServices
import com.appetiser.module.network.base.BaseRemoteSource
import com.appetiser.module.network.features.track.models.TrackDTO
import io.reactivex.Single
import javax.inject.Inject

class TrackRemoteSourceImpl @Inject constructor(
    private val baseplateApiServices: BaseplateApiServices
) : BaseRemoteSource(), TrackRemoteSource {

    override fun getTracks(): Single<List<Track>> {
        return baseplateApiServices
            .getTracks()
            .map { it.results }
            .map {
                it.map { trackDTO ->
                    TrackDTO.toDomain(trackDTO)
                }
            }
    }
}
