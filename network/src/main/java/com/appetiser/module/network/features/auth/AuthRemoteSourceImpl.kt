package com.appetiser.module.network.features.auth

import com.appetiser.module.domain.models.auth.CountryCode
import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.User
import com.appetiser.module.network.Stubs
import com.appetiser.module.network.base.BaseRemoteSource
import com.appetiser.module.network.BaseplateApiServices
import com.appetiser.module.network.features.user.models.UserDTO
import com.google.gson.Gson
import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class AuthRemoteSourceImpl @Inject constructor(
    private val baseplateApiServices: BaseplateApiServices,
    private val gson: Gson
) : BaseRemoteSource(), AuthRemoteSource {

    override fun logout(accessToken: AccessToken): Completable {
        return baseplateApiServices
            .logout(accessToken.bearerToken)
    }

    override fun forgotPassword(email: String): Single<Boolean> {
        val emailJson = JsonObject()
            .apply {
                addProperty("email", email)
            }
        val requestBody = getJsonRequestBody(emailJson.toString())

        return baseplateApiServices
            .forgotPassword(requestBody)
            .map { response ->
                response.success
            }
    }

    override fun forgotPasswordCheckCode(email: String, code: String): Single<Boolean> {
        val emailJson = JsonObject()
            .apply {
                addProperty("email", email)
                addProperty("token", code)
            }

        return baseplateApiServices
            .forgotPasswordCheckCode(getJsonRequestBody(emailJson.toString()))
            .map { it.success }
    }

    override fun newPassword(email: String, token: String, password: String, confirmPassword: String): Single<Boolean> {
        val passwordJson = JsonObject()
            .apply {
                addProperty("email", email)
                addProperty("token", token)
                addProperty("password", password)
                addProperty("password_confirmation", confirmPassword)
            }

        return baseplateApiServices
            .resetPassword(getJsonRequestBody(passwordJson.toString()))
            .map { it.success }
    }

    override fun checkUsername(username: String): Single<Boolean> {
        val userMap = hashMapOf<String, String>()
        userMap["username"] = username

        return baseplateApiServices
            .checkUsernameIfExists(userMap)
            .map { it.data.isEmailExists }
    }

    override fun login(username: String, password: String): Single<Pair<User, AccessToken>> {
        val userMap = hashMapOf<String, String>()
        userMap["username"] = username
        userMap["password"] = password

        return baseplateApiServices
            .login(userMap)
            .map { UserDTO.mapAuthDataResponse(it) }
    }

    /**
     * Call this method to login via social media.
     *
     * @param accessToken
     * @param accessTokenProvider provider of access token ["facebook", "google"].
     */
    override fun socialLogin(accessToken: String, accessTokenProvider: String): Single<Pair<User, AccessToken>> {
        val map = mapOf(
            "token" to accessToken,
            "provider" to accessTokenProvider
        )

        return baseplateApiServices
            .socialLogin(map)
            .map { UserDTO.mapAuthDataResponse(it) }
    }

    override fun register(
        email: String,
        phone: String,
        password: String,
        confirmPassword: String,
        firstName: String,
        lastName: String
    ): Single<Pair<User, AccessToken>> {
        val registerBody = JsonObject()
            .apply {
                if (email.isNotEmpty()) addProperty("email", email)
                if (phone.isNotEmpty()) addProperty("phone_number", phone)
                if (firstName.isNotEmpty()) addProperty("first_name", firstName)
                if (lastName.isNotEmpty()) addProperty("last_name", lastName)
                if (password.isNotEmpty()) addProperty("password", password)
                if (confirmPassword.isNotEmpty()) addProperty("password_confirmation", confirmPassword)
            }

        return baseplateApiServices
            .register(getJsonRequestBody(registerBody.toString()))
            .map { UserDTO.mapAuthDataResponse(it) }
    }

    override fun verifyAccountEmail(accessToken: AccessToken, code: String): Single<Pair<Boolean, User>> {
        val jsonBody = JsonObject()
            .apply {
                addProperty("token", code)
                addProperty("via", "email")
            }
        return baseplateApiServices
            .verifyAccount(
                accessToken.bearerToken,
                getJsonRequestBody(jsonBody.toString())
            )
            .map { UserDTO.toDomain(it.user) }
            .map { Pair(it.emailVerified, it) }
    }

    override fun verifyAccountMobilePhone(accessToken: AccessToken, code: String): Single<Pair<Boolean, User>> {
        val jsonBody = JsonObject()
            .apply {
                addProperty("token", code)
                addProperty("via", "phone_number")
            }
        return baseplateApiServices
            .verifyAccount(
                accessToken.bearerToken,
                getJsonRequestBody(jsonBody.toString())
            )
            .map { UserDTO.toDomain(it.user) }
            .map { Pair(it.emailVerified, it) }
    }

    override fun resendEmailVerificationCode(accessToken: AccessToken): Single<Boolean> {
        val jsonBody = JsonObject()
            .apply {
                addProperty("via", "email")
            }
        return baseplateApiServices
            .resendVerificationCode(
                accessToken.bearerToken,
                getJsonRequestBody(jsonBody.toString())
            )
            .map {
                it.success
            }
    }

    override fun resendMobilePhoneVerificationCode(accessToken: AccessToken): Single<Boolean> {
        val jsonBody = JsonObject()
            .apply {
                addProperty("via", "phone_number")
            }
        return baseplateApiServices
            .resendVerificationCode(
                accessToken.bearerToken,
                getJsonRequestBody(jsonBody.toString())
            )
            .map {
                it.success
            }
    }

    override fun getCountryCode(countryCodes: List<Int>): Single<List<CountryCode>> {
        val jsonObject = JsonObject()

        if (countryCodes.isNotEmpty()) {
            jsonObject
                .add(
                    "calling_codes",
                    gson.toJsonTree(countryCodes.toList()).asJsonArray
                )
        }

        val countryCode =
            if (countryCodes.isNotEmpty()) {
                baseplateApiServices
                    .getCountryCodes(
                        getJsonRequestBody(jsonObject.toString())
                    )
            } else {
                baseplateApiServices.getAllCountryCodes()
            }

        return countryCode
            .toObservable()
            .flatMapIterable {
                it.data.countries
            }
            .map {
                CountryCode(
                    id = it.id,
                    name = it.name,
                    callingCode = it.callingCode,
                    flag = it.flag.orEmpty()
                )
            }
            .toList()
            .onErrorResumeNext(Single.just(Stubs.countries))
    }
}
