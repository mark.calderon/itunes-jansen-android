package com.appetiser.module.network

import com.appetiser.baseplate.network.BuildConfig
import com.appetiser.module.network.features.track.TrackRemoteSource
import com.appetiser.module.network.features.track.TrackRemoteSourceImpl
import com.appetiser.module.network.features.user.UserRemoteSource
import com.appetiser.module.network.features.user.UserRemoteSourceImpl
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {
    @Provides
    @Singleton
    fun providesOkHttpClient(authenticatedInterceptor: Interceptor): OkHttpClient {

        val builder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            logging.redactHeader("Authorization")
            logging.redactHeader("Cookie")
            builder.addInterceptor(logging)
        }

        builder.addInterceptor(authenticatedInterceptor)

        return builder.build()
    }

    @Singleton
    @Provides
    fun providesCustomHeaderInterceptor(): Interceptor {
        return Interceptor { chain ->
            val request = chain.request()
                .newBuilder()
                .addHeader("Accept", "application/json")
                .addHeader("Content-Type", "application/json")
                .build()
            return@Interceptor chain.proceed(request)
        }
    }

    @Provides
    @Singleton
    fun providesGson(): Gson {
        return GsonBuilder()
            .setLenient()
            .create()
    }

    @Provides
    @Singleton
    fun providesRetrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
    }

    @Provides
    @Singleton
    fun providesUserRemoteSource(
        baseplateApiServices: BaseplateApiServices
    ): UserRemoteSource = UserRemoteSourceImpl(baseplateApiServices)

    @Provides
    @Singleton
    fun providesTrackRemoteSource(
        baseplateApiServices: BaseplateApiServices
    ): TrackRemoteSource = TrackRemoteSourceImpl(baseplateApiServices)
}
