package com.appetiser.module.network.features.auth.models.response

import com.appetiser.module.network.features.auth.models.RawCountryCode

open class CountryCodeDataResponse(
    val countries: List<RawCountryCode>
)
