package com.appetiser.module.domain.enums

enum class Kind {
    AUDIO_BOOK {
        override val value: String
            get() = "audiobook"
    },
    TV_SHOW {
        override val value: String
            get() = "tv-episode"
    },
    FILM {
        override val value: String
            get() = "feature-movie"
    },
    SONG {
        override val value: String
            get() = "song"
    };

    abstract val value: String
}