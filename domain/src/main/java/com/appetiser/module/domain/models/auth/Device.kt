package com.appetiser.module.domain.models.auth

data class Device(var width: Int, var height: Int)
