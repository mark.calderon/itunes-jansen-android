package com.appetiser.module.domain.models.notification

data class Notification(
    val id: String,
    val avatarUrl: String,
    val username: String,
    val message: String,
    val time: String,
    val imageUrl: String
)

