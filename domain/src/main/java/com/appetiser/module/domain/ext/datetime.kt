package com.appetiser.module.domain.ext

import com.appetiser.module.domain.enums.DateTimeFormat
import java.text.SimpleDateFormat
import java.util.*

fun Date.toReadableString(dateTimeFormat: DateTimeFormat): String? {
    val simpleDateFormat = SimpleDateFormat(dateTimeFormat.value, Locale.getDefault())
    return simpleDateFormat.format(this)
}

fun String.toDate(dateTimeFormat: DateTimeFormat): Date? = try {
    val simpleDateFormat = SimpleDateFormat(dateTimeFormat.value, Locale.getDefault())
    val date = this.replace("Z", "+00:00")
    simpleDateFormat.parse(date)
} catch (e: Throwable) {
    e.printStackTrace()
    null
}