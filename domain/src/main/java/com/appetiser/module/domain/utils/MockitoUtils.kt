package com.appetiser.module.domain.utils

import org.mockito.ArgumentCaptor
import org.mockito.Mockito

inline fun <reified T> any(): T = Mockito.any<T>()

inline fun <reified T> argumentCaptor(): ArgumentCaptor<T> = ArgumentCaptor.forClass(T::class.java)

fun <T> capture(argumentCaptor: ArgumentCaptor<T>): T = argumentCaptor.capture()

inline fun <reified T> mock(): T = Mockito.mock(T::class.java)
