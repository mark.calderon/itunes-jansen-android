package com.appetiser.module.domain.models.user

import com.google.gson.JsonObject

data class User(
    val id: String? = "",
    val email: String = "",
    val lastName: String? = "",
    val firstName: String? = "",
    val fullName: String? = "",
    val avatarPermanentUrl: String? = "",
    val avatarPermanentThumbUrl: String? = "",
    val phoneNumber: String = "",
    val emailVerified: Boolean = false,
    val phoneNumberVerified: Boolean = false,
    val verified: Boolean = false
) {
    companion object {
        fun empty(): User {
            return User()
        }
    }

    fun toJsonStringExcludeEmpty(): String {
        val userJson = JsonObject()
            .apply {
                if (email.isNotEmpty()) addProperty("email", email)
                if (phoneNumber.isNotEmpty()) addProperty("phone_number", phoneNumber)
                if (!lastName.isNullOrEmpty()) addProperty("last_name", lastName)
                if (!firstName.isNullOrEmpty()) addProperty("first_name", firstName)
                if (!fullName.isNullOrEmpty()) addProperty("full_name", fullName)
            }

        return userJson
            .toString()
    }

    /**
     * Returns true if user onboarding details are present.
     */
    fun hasOnboardingDetails(): Boolean {
        return !fullName.isNullOrEmpty()
    }
}
