package com.appetiser.module.notification.fcm

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import dagger.android.AndroidInjection
import timber.log.Timber

class BaseplateFirebaseMessagingService : FirebaseMessagingService() {

    override fun onCreate() {
        AndroidInjection.inject(this)
        super.onCreate()
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        val notification = remoteMessage.notification

        if (notification != null) {
            // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
            Timber.d("From: %s", remoteMessage.from)
            Timber.d("Message Notification Title: %s", remoteMessage.notification?.title)
            Timber.d("Message Notification Body: %s", remoteMessage.notification?.body)
            // TODO uncomment if you want to use `data
            // val data = remoteMessage.data

            Timber.d("Message Notification Body: %s", remoteMessage.notification?.body)
        }
    }

    override fun onNewToken(token: String) {
        Timber.d("Refreshed token: %s", token)
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token)
    }

    private fun sendRegistrationToServer(token: String) {
    }
}
