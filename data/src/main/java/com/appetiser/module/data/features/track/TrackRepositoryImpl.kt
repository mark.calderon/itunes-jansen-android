package com.appetiser.module.data.features.track

import com.appetiser.module.domain.models.track.Track
import com.appetiser.module.local.features.tracks.TrackLocalSource
import com.appetiser.module.network.features.track.TrackRemoteSource
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class TrackRepositoryImpl @Inject constructor(
    private val trackLocalSource: TrackLocalSource,
    private val tracksRemoteSource: TrackRemoteSource
) : TrackRepository {

    override fun getTracks(): Flowable<List<Track>> = trackLocalSource
        .getTracks()
        .take(1)
        .switchMap {
            when {
                it.isEmpty() -> {
                    tracksRemoteSource.getTracks().toFlowable().switchMap { tracksFromRemote ->
                        trackLocalSource.saveAll(tracksFromRemote).toFlowable()
                    }
                }
                else -> Flowable.just(it)
            }
        }

    override fun getTrack(id: Int): Single<Track> = trackLocalSource.getTrack(id)
}
