package com.appetiser.module.data.features.auth

import com.appetiser.module.domain.models.Session
import com.appetiser.module.domain.models.auth.CountryCode
import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.User
import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.network.features.auth.AuthRemoteSource
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
    private val remote: AuthRemoteSource,
    private val sessionLocalSource: SessionLocalSource
) : AuthRepository {

    override fun checkUsername(username: String): Single<Boolean> = remote.checkUsername(username)

    override fun login(username: String, password: String): Single<Session> {
        return remote
            .login(username, password)
            .flatMap(::saveAuthDataToSession)
    }

    /**
     * Call this method to login via social media.
     *
     * @param accessToken
     * @param accessTokenProvider provider of access token ["facebook", "google"].
     */
    override fun socialLogin(
        accessToken: String,
        accessTokenProvider: String
    ): Single<Session> {
        return remote
            .socialLogin(accessToken, accessTokenProvider)
            .flatMap(::saveAuthDataToSession)
    }

    override fun register(
        email: String,
        phone: String,
        password: String,
        confirmPassword: String,
        firstName: String,
        lastName: String
    ): Single<Session> {
        return remote
            .register(
                email,
                phone,
                password,
                confirmPassword,
                firstName,
                lastName
            )
            .flatMap(::saveAuthDataToSession)
    }

    private fun saveAuthDataToSession(pair: Pair<User, AccessToken>): Single<Session> {
        val (user, accessToken) = pair

        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                session.user = user
                session.accessToken = accessToken

                sessionLocalSource
                    .saveSession(session)
            }
    }

    override fun verifyAccountEmail(code: String): Single<Boolean> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                remote
                    .verifyAccountEmail(
                        session.accessToken,
                        code
                    )
            }
            .flatMap { (isSuccessful, user) ->
                if (isSuccessful) {
                    // If verification succeeds. Save user to database.
                    sessionLocalSource
                        .getSession()
                        .flatMap { session ->
                            session.user = user

                            sessionLocalSource
                                .saveSession(session)
                        }
                        .map { isSuccessful }
                } else {
                    Single.just(isSuccessful)
                }
            }
    }

    override fun verifyAccountMobilePhone(code: String): Single<Boolean> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                remote
                    .verifyAccountMobilePhone(
                        session.accessToken,
                        code
                    )
            }
            .flatMap { (isSuccessful, user) ->
                if (isSuccessful) {
                    // If verification succeeds. Save user to database.
                    sessionLocalSource
                        .getSession()
                        .flatMap { session ->
                            session.user = user

                            sessionLocalSource
                                .saveSession(session)
                        }
                        .map { isSuccessful }
                } else {
                    Single.just(isSuccessful)
                }
            }
    }

    override fun resendEmailVerificationCode(): Single<Boolean> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                remote
                    .resendEmailVerificationCode(
                        session.accessToken
                    )
            }
    }

    override fun resendMobilePhoneVerificationCode(): Single<Boolean> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                remote
                    .resendMobilePhoneVerificationCode(
                        session.accessToken
                    )
            }
    }

    override fun forgotPassword(email: String): Single<Boolean> {
        return remote.forgotPassword(email)
    }

    override fun forgotPasswordCheckCode(email: String, code: String): Single<Boolean> {
        return remote.forgotPasswordCheckCode(email, code)
    }

    override fun newPassword(
        email: String,
        token: String,
        password: String,
        confirmPassword: String
    ): Single<Boolean> {
        return remote.newPassword(email, token, password, confirmPassword)
    }

    override fun logout(): Completable {
        return sessionLocalSource
            .getSession()
            .flatMapCompletable { session ->
                remote
                    .logout(session.accessToken)
            }
            .andThen(sessionLocalSource.clearSession())
    }

    override fun getCountryCode(countryCodes: List<Int>): Single<List<CountryCode>> {
        return remote.getCountryCode(countryCodes)
    }
}
