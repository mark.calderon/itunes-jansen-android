package com.appetiser.module.data.features

import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.module.data.features.session.SessionRepositoryImpl
import com.appetiser.module.data.features.track.TrackRepository
import com.appetiser.module.data.features.track.TrackRepositoryImpl
import com.appetiser.module.data.features.user.UserRepository
import com.appetiser.module.data.features.user.UserRepositoryImpl
import com.appetiser.module.data.features.visit.VisitTimeRepository
import com.appetiser.module.data.features.visit.VisitTimeRepositoryImpl
import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.local.features.tracks.TrackLocalSource
import com.appetiser.module.local.features.visit.VisitTimeLocalSource
import com.appetiser.module.network.features.track.TrackRemoteSource
import com.appetiser.module.network.features.user.UserRemoteSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {
    @Provides
    @Singleton
    fun providesSessionRepository(
        sessionLocalSource: SessionLocalSource
    ): SessionRepository {
        return SessionRepositoryImpl(sessionLocalSource)
    }

    @Provides
    @Singleton
    fun providesUserRepository(
        sessionLocalSource: SessionLocalSource,
        userRemoteSource: UserRemoteSource
    ): UserRepository {
        return UserRepositoryImpl(sessionLocalSource, userRemoteSource)
    }

    @Provides
    @Singleton
    fun providesTrackRepository(
        trackLocalSource: TrackLocalSource,
        trackRemoteSource: TrackRemoteSource
    ): TrackRepository {
        return TrackRepositoryImpl(trackLocalSource, trackRemoteSource)
    }

    @Provides
    @Singleton
    fun providesVisitTimeRepository(
        visitTimeLocalSource: VisitTimeLocalSource
    ): VisitTimeRepository {
        return VisitTimeRepositoryImpl(visitTimeLocalSource)
    }
}
