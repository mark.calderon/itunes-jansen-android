package com.appetiser.module.data.features.visit

import com.appetiser.module.local.features.visit.VisitTimeLocalSource
import javax.inject.Inject

class VisitTimeRepositoryImpl @Inject constructor(
    private val visitTimeLocalSource: VisitTimeLocalSource
) : VisitTimeRepository {

    override fun saveVisitTime(time: String) {
        visitTimeLocalSource.saveVisitTime(time)
    }

    override fun getVisitTime(): String = visitTimeLocalSource.getVisitTime()
}
