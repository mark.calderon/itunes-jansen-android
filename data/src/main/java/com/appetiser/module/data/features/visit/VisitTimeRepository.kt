package com.appetiser.module.data.features.visit

interface VisitTimeRepository {

    fun saveVisitTime(time: String)

    fun getVisitTime(): String
}
