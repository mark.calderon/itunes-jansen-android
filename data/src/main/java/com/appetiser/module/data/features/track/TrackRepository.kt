package com.appetiser.module.data.features.track

import com.appetiser.module.domain.models.track.Track
import io.reactivex.Flowable
import io.reactivex.Single

interface TrackRepository {

    fun getTracks(): Flowable<List<Track>>

    fun getTrack(id: Int): Single<Track>
}
