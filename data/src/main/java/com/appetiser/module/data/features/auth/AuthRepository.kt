package com.appetiser.module.data.features.auth

import com.appetiser.module.domain.models.Session
import com.appetiser.module.domain.models.auth.CountryCode
import io.reactivex.Completable
import io.reactivex.Single

interface AuthRepository {

    /**
     * Checks if username has an existing account in backend.
     *
     * @param username email or phone number
     */
    fun checkUsername(username: String): Single<Boolean>

    fun login(username: String, password: String): Single<Session>

    /**
     * Call this method to login via social media.
     *
     * @param accessToken
     * @param accessTokenProvider provider of access token ["facebook", "google"].
     */
    fun socialLogin(
        accessToken: String,
        accessTokenProvider: String
    ): Single<Session>

    /**
     * If user uses phone number for logging in. Pass only [phone].
     */
    fun register(
        email: String = "",
        phone: String = "",
        password: String,
        confirmPassword: String,
        firstName: String,
        lastName: String
    ): Single<Session>

    fun verifyAccountEmail(code: String): Single<Boolean>

    fun verifyAccountMobilePhone(code: String): Single<Boolean>

    fun resendEmailVerificationCode(): Single<Boolean>

    fun resendMobilePhoneVerificationCode(): Single<Boolean>

    fun forgotPassword(email: String): Single<Boolean>

    fun forgotPasswordCheckCode(email: String, code: String): Single<Boolean>

    fun newPassword(email: String, token: String, password: String, confirmPassword: String): Single<Boolean>

    fun getCountryCode(countryCodes: List<Int>): Single<List<CountryCode>>

    fun logout(): Completable
}
