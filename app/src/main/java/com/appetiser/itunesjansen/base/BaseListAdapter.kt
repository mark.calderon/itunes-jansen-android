package com.appetiser.itunesjansen.base

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.databinding.library.baseAdapters.BR

abstract class BaseListAdapter<T, VH : BaseListAdapter.BaseViewViewHolder<T>>(
    diffUtil: DiffUtil.ItemCallback<T>
) : ListAdapter<T, VH>(diffUtil) {

    val items: MutableList<T> = mutableListOf()

    open class BaseViewViewHolder<T>(open val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

        open fun bind(item: T) {
            binding.setVariable(BR.item, item)
            binding.executePendingBindings()
        }
    }

    open fun updateItems(items: List<T>) {
        this.items.clear()
        this.items.addAll(items.toMutableList())
        submitList(this.items)
    }
}
