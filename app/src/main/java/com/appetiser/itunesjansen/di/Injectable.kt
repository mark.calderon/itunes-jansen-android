package com.appetiser.itunesjansen.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
