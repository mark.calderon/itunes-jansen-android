package com.appetiser.itunesjansen.di.builders

import com.appetiser.itunesjansen.di.scopes.ServiceScope
import com.appetiser.module.notification.fcm.BaseplateFirebaseMessagingService
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ServiceBuilder {

    @ServiceScope
    @ContributesAndroidInjector
    abstract fun contributeBaseplateFirebaseMessagingService(): BaseplateFirebaseMessagingService
}
