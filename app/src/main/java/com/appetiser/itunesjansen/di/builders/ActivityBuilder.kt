package com.appetiser.itunesjansen.di.builders

import com.appetiser.itunesjansen.di.scopes.ActivityScope
import com.appetiser.itunesjansen.features.auth.AuthRepositoryModule
import com.appetiser.itunesjansen.features.auth.emailcheck.EmailCheckActivity
import com.appetiser.itunesjansen.features.auth.forgotpassword.ForgotPasswordActivity
import com.appetiser.itunesjansen.features.auth.forgotpassword.newpassword.NewPasswordActivity
import com.appetiser.itunesjansen.features.auth.forgotpassword.verification.ForgotPasswordVerificationActivity
import com.appetiser.itunesjansen.features.auth.landing.LandingActivity
import com.appetiser.itunesjansen.features.auth.login.LoginActivity
import com.appetiser.itunesjansen.features.auth.register.RegisterActivity
import com.appetiser.itunesjansen.features.auth.register.createpassword.CreatePasswordActivity
import com.appetiser.itunesjansen.features.auth.register.details.UserDetailsActivity
import com.appetiser.itunesjansen.features.auth.register.profile.UploadProfilePhotoActivity
import com.appetiser.itunesjansen.features.auth.register.verification.RegisterVerificationCodeActivity
import com.appetiser.itunesjansen.features.auth.subscription.SubscriptionActivity
import com.appetiser.itunesjansen.features.auth.walkthrough.WalkthroughActivity
import com.appetiser.itunesjansen.features.main.MainActivity
import com.appetiser.itunesjansen.features.track.TrackListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeEmailCheckActivity(): EmailCheckActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeLoginActivity(): LoginActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeForgotPasswordActivity(): ForgotPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeRegisterVerificationActivity(): RegisterVerificationCodeActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeRegisterActivity(): RegisterActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeForgotPasswordVerificationActivity(): ForgotPasswordVerificationActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeNewPasswordActivity(): NewPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeUserDetailsActivity(): UserDetailsActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeMainActivity(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeLandingActivity(): LandingActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeWalkthroughActivity(): WalkthroughActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeCreatePasswordActivity(): CreatePasswordActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeUploadProfilePhotoActivity(): UploadProfilePhotoActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeSubscriptionActivity(): SubscriptionActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeTrackListActivity(): TrackListActivity
}
