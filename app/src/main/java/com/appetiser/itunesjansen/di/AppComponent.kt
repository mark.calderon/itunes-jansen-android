package com.appetiser.itunesjansen.di

import android.app.Application
import com.appetiser.itunesjansen.ItunesJansenApplication
import com.appetiser.itunesjansen.di.builders.ActivityBuilder
import com.appetiser.itunesjansen.di.builders.FragmentBuilder
import com.appetiser.itunesjansen.di.builders.ServiceBuilder
import com.appetiser.module.data.features.RepositoryModule
import com.appetiser.module.local.StorageModule
import com.appetiser.module.network.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton
import com.appetiser.module.local.DatabaseModule
import com.appetiser.module.network.ApiServiceModule

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        StorageModule::class,
        FragmentBuilder::class,
        DatabaseModule::class,
        NetworkModule::class,
        ApiServiceModule::class,
        ActivityBuilder::class,
        SchedulerModule::class,
        ServiceBuilder::class,
        RepositoryModule::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: ItunesJansenApplication)
}
