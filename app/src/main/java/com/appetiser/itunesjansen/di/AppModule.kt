package com.appetiser.itunesjansen.di

import android.content.Context
import com.appetiser.itunesjansen.ItunesJansenApplication
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class AppModule {

    @Singleton
    @Binds
    abstract fun providesApplicationContext(app: ItunesJansenApplication): Context
}
