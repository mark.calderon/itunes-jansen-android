package com.appetiser.itunesjansen.di

import com.appetiser.itunesjansen.utils.schedulers.BaseSchedulerProvider
import com.appetiser.itunesjansen.utils.schedulers.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SchedulerModule {

    @Provides
    @Singleton
    fun providesSchedulerSource(): BaseSchedulerProvider =
            SchedulerProvider.getInstance()
}
