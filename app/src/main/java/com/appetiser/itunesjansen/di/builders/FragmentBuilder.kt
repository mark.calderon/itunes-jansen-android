package com.appetiser.itunesjansen.di.builders

import com.appetiser.itunesjansen.di.scopes.FragmentScope
import com.appetiser.itunesjansen.features.main.DummyFragment
import com.appetiser.itunesjansen.features.notification.NotificationFragment
import com.appetiser.itunesjansen.features.track.TrackListFragment
import com.appetiser.itunesjansen.features.track.detail.TrackDetailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeNotificationFragment(): NotificationFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeDummyFragment(): DummyFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeTrackListFragment(): TrackListFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeTrackDetailFragment(): TrackDetailFragment
}
