package com.appetiser.itunesjansen.ext

import android.view.View

/**
 * Makes this view visible if condition is true, gone otherwise.
 */
fun View.makeVisibleOrGone(condition: Boolean) {
    when (condition) {
        true -> this.makeVisible()
        else -> this.makeGone()
    }
}

/**
 * Makes this view visible if condition is true, gone otherwise.
 */
fun View.makeVisibleOrGone(condition: () -> Boolean) {
    this.makeVisibleOrGone(condition())
}

/**
 * Makes the view visible
 */
fun View.makeVisible() {
    visibility = View.VISIBLE
}

/**
 * Makes the view gone
 */
fun View.makeGone() {
    visibility = View.GONE
}

/**
 * Makes the view invisible
 */
fun View.makeInvisible() {
    visibility = View.INVISIBLE
}
