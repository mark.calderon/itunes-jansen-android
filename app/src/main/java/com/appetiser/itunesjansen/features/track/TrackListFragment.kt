package com.appetiser.itunesjansen.features.track

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.appetiser.itunesjansen.R
import com.appetiser.itunesjansen.base.BaseViewModelFragment
import com.appetiser.itunesjansen.databinding.FragmentTrackListBinding
import com.appetiser.itunesjansen.ext.getThrowableError
import com.appetiser.itunesjansen.ext.toDate
import com.appetiser.itunesjansen.ext.toRelativeDateTime
import com.appetiser.module.common.setVisible
import com.appetiser.module.common.toast
import com.appetiser.module.domain.enums.DateTimeFormat
import com.appetiser.module.domain.models.track.Track
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class TrackListFragment : BaseViewModelFragment<FragmentTrackListBinding, TrackListViewModel>() {

    companion object {
        fun newInstance() = TrackListFragment()
    }

    private val adapterMovies: TrackListAdapter by lazy {
        TrackListAdapter(::onItemClick)
    }

    private val adapterSongs: TrackListAdapter by lazy {
        TrackListAdapter(::onItemClick)
    }

    private val adapterAudioBooks: TrackListAdapter by lazy {
        TrackListAdapter(::onItemClick)
    }

    private val adapterTVShows: TrackListAdapter by lazy {
        TrackListAdapter(::onItemClick)
    }

    private fun onItemClick(track: Track) {
        when (findNavController().currentDestination?.id) {
            R.id.trackListFragment -> findNavController()
                .navigate(
                    TrackListFragmentDirections.goToTrackDetailFragment(
                        track.collectionId
                    )
                )
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_track_list

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        setupViewModel()
    }

    private fun setupViews() {
        binding.recyclerViewFilms.apply {
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            adapter = adapterMovies
        }

        binding.recyclerViewSongs.apply {
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            adapter = adapterSongs
        }

        binding.recyclerViewTvShows.apply {
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            adapter = adapterTVShows
        }

        binding.recyclerViewAudiobooks.apply {
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            adapter = adapterAudioBooks
        }

        binding.swipeRefreshLayout.setOnRefreshListener {
            viewModel.getAllTracks()
        }
    }

    private fun setupViewModel() {
        viewModel
            .state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }

    private fun handleState(state: TrackListState) {
        when (state) {
            is TrackListState.LastVisitTime -> setLastVisitTime(state.visitTime)
            is TrackListState.Error -> activity?.toast(state.throwable.getThrowableError())
            is TrackListState.TracksSongs -> adapterSongs.updateItems(state.tracks)
            is TrackListState.TracksFilms -> adapterMovies.updateItems(state.tracks)
            is TrackListState.TracksTvShows -> adapterTVShows.updateItems(state.tracks)
            is TrackListState.TracksAudiobooks -> adapterAudioBooks.updateItems(state.tracks)

            TrackListState.ShowProgressLoading -> binding.swipeRefreshLayout.isRefreshing = true
            TrackListState.HideProgressLoading -> binding.swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun setLastVisitTime(visitTime: String) {
        binding.textViewLastVisited.apply {
            val visitTimeDisplay = visitTime.toDate(DateTimeFormat.DATE_TIME)?.toRelativeDateTime(context)
            text = String.format(resources.getString(R.string.last_visited_time), visitTimeDisplay)
            setVisible(visitTime.isNotEmpty())
        }
    }
}
