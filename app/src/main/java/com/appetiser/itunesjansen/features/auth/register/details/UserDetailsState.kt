package com.appetiser.itunesjansen.features.auth.register.details

import com.appetiser.module.domain.models.user.User

sealed class UserDetailsState {

    data class UserDetailsUpdated(val user: User) : UserDetailsState()

    data class Error(val throwable: Throwable) : UserDetailsState()

    object ShowProgressLoading : UserDetailsState()

    object HideProgressLoading : UserDetailsState()
}
