package com.appetiser.itunesjansen.features.auth.forgotpassword.newpassword

import android.os.Bundle
import com.appetiser.itunesjansen.base.BaseViewModel
import com.appetiser.module.common.toObservable
import com.appetiser.module.data.features.auth.AuthRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class NewPasswordViewModel @Inject constructor(
    private val repository: AuthRepository
) : BaseViewModel() {

    private lateinit var email: String
    private lateinit var token: String

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        email = bundle?.getString(NewPasswordActivity.KEY_EMAIL, "").orEmpty()
        token = bundle?.getString(NewPasswordActivity.KEY_TOKEN, "").orEmpty()

        email.toObservable {
            _state.onNext(NewPasswordState.GetEmail(it))
        }
    }

    private val _state by lazy {
        PublishSubject.create<NewPasswordState>()
    }

    val state: Observable<NewPasswordState> = _state

    fun sendNewPassword(password: String) {
        _state.onNext(NewPasswordState.ShowProgressLoading)
        disposables.add(repository.newPassword(email, token, password, password)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(NewPasswordState.ShowProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(NewPasswordState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(NewPasswordState.HideProgressLoading)
            }
            .subscribeBy(onSuccess = {
                _state.onNext(NewPasswordState.Success(email))
            }, onError = {
                _state.onNext(NewPasswordState.Error(it))
            }))
    }
}
