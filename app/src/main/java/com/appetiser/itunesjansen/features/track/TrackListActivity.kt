package com.appetiser.itunesjansen.features.track

import android.os.Bundle
import com.appetiser.itunesjansen.R
import dagger.android.support.DaggerAppCompatActivity

class TrackListActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_track_list)
    }
}
