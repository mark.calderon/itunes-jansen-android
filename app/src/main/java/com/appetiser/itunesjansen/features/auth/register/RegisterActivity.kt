package com.appetiser.itunesjansen.features.auth.register

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import com.appetiser.auth_modern.databinding.ActivityMobileNumberBinding
import com.appetiser.itunesjansen.R
import com.appetiser.itunesjansen.base.BaseViewModelActivity
import com.appetiser.itunesjansen.ext.disabledWithAlpha
import com.appetiser.itunesjansen.ext.enabledWithAlpha
import com.appetiser.itunesjansen.ext.getThrowableError
import com.appetiser.itunesjansen.features.auth.register.verification.RegisterVerificationCodeActivity
import com.appetiser.module.common.NINJA_TAP_THROTTLE_TIME
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.toast
import com.jakewharton.rxbinding3.widget.textChangeEvents
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class RegisterActivity : BaseViewModelActivity<ActivityMobileNumberBinding, RegisterViewModel>() {

    companion object {
        fun openActivity(context: Context, email: String, password: String) {
            val intent = Intent(context, RegisterActivity::class.java)
            intent.putExtra(KEY_EMAIL, email)
            intent.putExtra(KEY_PASSWORD, password)
            context.startActivity(intent)
        }

        const val KEY_EMAIL = "email"
        const val KEY_PASSWORD = "password"
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_mobile_number
    }

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupViews()
        setupToolbar()
        setupViewModel()
        observeInputViews()
    }

    private fun observeInputViews() {
        val mobileObservable = binding.etMobile.textChangeEvents()
            .skipInitialValue()
            .map { it.text }
            .map { it.isNotEmpty() }

        disposables.add(
            mobileObservable
                .debounce(NINJA_TAP_THROTTLE_TIME, TimeUnit.MILLISECONDS)
                .observeOn(scheduler.ui())
                .subscribe({
                    if (it) {
                        binding.btnContinue.enabledWithAlpha()
                    } else {
                        binding.btnContinue.disabledWithAlpha()
                    }
                }, {
                    Timber.e(it)
                })
        )
    }

    private fun setupViewModel() {
        viewModel.state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { state ->
                    when (state) {

                        is RegisterState.SaveLoginCredentials -> {
                            handleSuccessRegistration()
                        }

                        is RegisterState.Error -> {
                            toast(state.throwable.getThrowableError())
                        }

                        is RegisterState.ShowProgressLoading -> {
                            toast("Sending request")
                        }

                        is RegisterState.HideProgressLoading -> {
                        }
                    }
                },
                onError = {
                    Timber.e("Error $it")
                }
            )
            .apply {
                disposables.add(this)
            }
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }

    private fun setupViews() {
        binding.etMobile.apply {
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    requestVerificationCode()
                    true
                } else {
                    false
                }
            }
        }

        disposables.add(binding.btnContinue.ninjaTap {
            requestVerificationCode()
        })
    }

    private fun requestVerificationCode() {
        val mobileNo = "${binding.countryCodePicker.selectedCountryCodeWithPlus}${binding.etMobile.text}"
        viewModel.register(mobileNo)
    }

    private fun handleSuccessRegistration() {
        RegisterVerificationCodeActivity
            .openActivity(
                this,
                email = viewModel.email
            )
        finish()
    }
}
