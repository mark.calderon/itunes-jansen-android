package com.appetiser.itunesjansen.features.auth

import com.appetiser.itunesjansen.di.scopes.ActivityScope
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.data.features.auth.AuthRepositoryImpl
import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.network.BaseplateApiServices
import com.appetiser.module.network.features.auth.AuthRemoteSource
import com.appetiser.module.network.features.auth.AuthRemoteSourceImpl
import com.google.gson.Gson
import dagger.Module
import dagger.Provides

@Module
class AuthRepositoryModule {

    @Provides
    fun providesAuthRemoteSource(
        baseplateApiServices: BaseplateApiServices,
        gson: Gson
    ): AuthRemoteSource = AuthRemoteSourceImpl(baseplateApiServices, gson)

    @ActivityScope
    @Provides
    fun providesAuthRepository(
        remote: AuthRemoteSource,
        sessionLocalSource: SessionLocalSource
    ): AuthRepository = AuthRepositoryImpl(remote, sessionLocalSource)
}
