package com.appetiser.itunesjansen.features.auth.register.details

import android.os.Bundle
import com.appetiser.itunesjansen.base.BaseViewModel
import com.appetiser.module.data.features.user.UserRepository
import com.appetiser.module.domain.models.user.User
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class UserDetailsViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    private val _state by lazy {
        PublishSubject.create<UserDetailsState>()
    }

    val state: Observable<UserDetailsState> = _state

    fun sendUserDetails(firstName: String, lastName: String) {
        userRepository
            .updateUser(
                User(
                    firstName = firstName,
                    lastName = lastName
                )
            )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(UserDetailsState.ShowProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(UserDetailsState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(UserDetailsState.HideProgressLoading)
            }
            .subscribeBy(
                onSuccess = { user ->
                    if (!user.firstName.isNullOrEmpty() && !user.lastName.isNullOrEmpty()) {
                        _state.onNext(UserDetailsState.UserDetailsUpdated(user))
                    }
                }, onError = {
                    _state.onNext(UserDetailsState.Error(it))
                }
            )
            .apply { disposables.add(this) }
    }

    fun sendUserDetails(fullName: String) {
        userRepository
            .updateUser(
                User(fullName = fullName)
            )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(UserDetailsState.ShowProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(UserDetailsState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(UserDetailsState.HideProgressLoading)
            }
            .subscribeBy(
                onSuccess = { user ->
                    if (!user.fullName.isNullOrEmpty()) {
                        _state.onNext(UserDetailsState.UserDetailsUpdated(user))
                    } else {
                        _state.onNext(UserDetailsState.Error(Throwable("Something went wrong")))
                    }
                }, onError = {
                    _state.onNext(UserDetailsState.Error(it))
                }
            )
            .apply { disposables.add(this) }
    }
}
