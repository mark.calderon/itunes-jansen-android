package com.appetiser.itunesjansen.features.auth.register

import com.appetiser.module.domain.models.auth.CountryCode
import com.appetiser.module.domain.models.user.User

sealed class RegisterState {

    data class GetEmail(val email: String) : RegisterState()

    data class SaveLoginCredentials(val user: User) : RegisterState()

    data class GetCountryCode(val countryCode: List<CountryCode>) : RegisterState()

    data class Error(val throwable: Throwable) : RegisterState()

    object ShowProgressLoading : RegisterState()

    object HideProgressLoading : RegisterState()
}
