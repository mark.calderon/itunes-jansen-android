package com.appetiser.itunesjansen.features.notification

import com.appetiser.module.domain.models.notification.Notification

data class NotificationViewTypeItem(val type: NotificationType, val notification: Notification? = null, val title: String?) {

    val usernameLength: Int = notification?.username?.length ?: 0
    val messageLength: Int = notification?.message?.length ?: 0
    val timeLength: Int = notification?.time?.length ?: 0
}

enum class NotificationType(type: Int) {
    HEADER(0), NORMAL(1), IMAGE(2)
}
