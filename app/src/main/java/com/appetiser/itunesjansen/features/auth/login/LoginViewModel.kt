package com.appetiser.itunesjansen.features.auth.login

import android.os.Bundle
import com.appetiser.itunesjansen.base.BaseViewModel
import com.appetiser.module.data.features.auth.AuthRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val repository: AuthRepository
) : BaseViewModel() {

    private var email: String = ""
    private var phone: String = ""

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        email = bundle?.getString(LoginActivity.KEY_EMAIL, "").orEmpty()
        phone = bundle?.getString(LoginActivity.KEY_PHONE, "").orEmpty()
    }

    private val _state by lazy {
        PublishSubject.create<LoginState>()
    }

    val state: Observable<LoginState> = _state

    fun requestArguments() {
        val username = when {
            email.isNotEmpty() -> email
            phone.isNotEmpty() -> phone
            else -> ""
        }

        if (username.isEmpty()) return

        _state
            .onNext(
                LoginState.GetUsername(username)
            )
    }

    fun login(email: String, password: String) {
        repository
            .login(email, password)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(LoginState.ShowProgressLoading)
            }
            .subscribeBy(
                onSuccess = { session ->
                    _state.onNext(LoginState.HideProgressLoading)

                    val user = session.user

                    if (user.id.orEmpty().isNotEmpty()) {
                        if (user.verified) {
                            if (user.hasOnboardingDetails()) {
                                _state.onNext(LoginState.LoginSuccess(user))
                            } else {
                                _state.onNext(LoginState.NoUserFirstAndLastName)
                            }
                        } else {
                            _state.onNext(
                                LoginState
                                    .UserNotVerified(
                                        user,
                                        user.email,
                                        user.phoneNumber
                                    )
                            )
                        }
                    }
                }, onError = {
                    _state.onNext(LoginState.Error(it))
                })

            .apply { disposables.add(this) }
    }
}
