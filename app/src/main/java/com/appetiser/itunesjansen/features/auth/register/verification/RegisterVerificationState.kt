package com.appetiser.itunesjansen.features.auth.register.verification

sealed class RegisterVerificationState {
    data class FillUsername(val username: String) : RegisterVerificationState()

    data class SendCodeSuccess(val token: String) : RegisterVerificationState()

    object ResendCodeSuccess : RegisterVerificationState()

    data class Error(val throwable: Throwable) : RegisterVerificationState()

    object ShowProgressLoading : RegisterVerificationState()

    object HideProgressLoading : RegisterVerificationState()

    data class RegisterSuccess(val userId: String) : RegisterVerificationState()
}
