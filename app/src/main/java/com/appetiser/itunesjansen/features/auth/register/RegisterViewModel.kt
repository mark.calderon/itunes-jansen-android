package com.appetiser.itunesjansen.features.auth.register

import android.os.Bundle
import com.appetiser.itunesjansen.base.BaseViewModel
import com.appetiser.module.data.features.auth.AuthRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class RegisterViewModel @Inject constructor(
    private val repository: AuthRepository
) : BaseViewModel() {

    lateinit var email: String
    private lateinit var password: String

    private val _state by lazy {
        PublishSubject.create<RegisterState>()
    }

    val state: Observable<RegisterState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        email = bundle?.getString(RegisterActivity.KEY_EMAIL, "").orEmpty()
        password = bundle?.getString(RegisterActivity.KEY_PASSWORD, "").orEmpty()

//        email.toObservable {
//            _state.onNext(RegisterState.GetEmail(it))
//        }
    }

    fun setEmailAndPassword(email: String, password: String) {
        this.email = email
        this.password = password
    }

    fun register(mobileNumber: String) {
        repository
            .register(
                email = email,
                phone = mobileNumber,
                password = password,
                confirmPassword = password,
                firstName = "",
                lastName = ""
            )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(RegisterState.ShowProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(RegisterState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(RegisterState.HideProgressLoading)
            }
            .subscribeBy(
                onSuccess = { session ->
                    val user = session.user

                    if (user.id.orEmpty().isNotEmpty()) {
                        _state.onNext(RegisterState.SaveLoginCredentials(user))
                    }
                },
                onError = {
                    _state.onNext(RegisterState.Error(it))
                }
            )
            .addTo(disposables)
    }
}
