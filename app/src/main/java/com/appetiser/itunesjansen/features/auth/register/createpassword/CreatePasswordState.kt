package com.appetiser.itunesjansen.features.auth.register.createpassword

sealed class CreatePasswordState {
    data class FillUsername(
        val username: String
    ) : CreatePasswordState()

    data class StartRegisterScreen(
        val password: String,
        val email: String
    ) : CreatePasswordState()

    data class StartVerificationCodeScreen(
        val phone: String
    ) : CreatePasswordState()

    object PasswordBelowMinLength : CreatePasswordState()

    object ShowLoading : CreatePasswordState()

    object HideLoading : CreatePasswordState()

    object EnableButton : CreatePasswordState()

    object DisableButton : CreatePasswordState()
}
