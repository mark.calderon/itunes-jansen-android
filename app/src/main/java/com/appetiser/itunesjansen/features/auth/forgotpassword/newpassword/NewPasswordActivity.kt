package com.appetiser.itunesjansen.features.auth.forgotpassword.newpassword

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import com.appetiser.auth_modern.databinding.ActivityNewPasswordBinding
import com.appetiser.itunesjansen.R
import com.appetiser.itunesjansen.base.BaseViewModelActivity
import com.appetiser.itunesjansen.ext.enableWithAplhaWhen
import com.appetiser.itunesjansen.ext.getThrowableError
import com.appetiser.itunesjansen.features.auth.login.LoginActivity
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.toast
import com.appetiser.module.common.widget.CustomPasswordTransformation
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class NewPasswordActivity : BaseViewModelActivity<ActivityNewPasswordBinding, NewPasswordViewModel>() {

    companion object {
        fun openActivity(context: Context, email: String, token: String) {
            val intent = Intent(context, NewPasswordActivity::class.java)
            intent.putExtra(KEY_EMAIL, email)
            intent.putExtra(KEY_TOKEN, token)
            context.startActivity(intent)
        }

        const val KEY_EMAIL = "email"
        const val KEY_TOKEN = "token"
    }

    override fun getLayoutId(): Int = R.layout.activity_new_password

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupViews()
        setupToolbar()
        setupViewModels()
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }

    private fun setupViews() {
        binding.etPassword.apply {
            transformationMethod = CustomPasswordTransformation()
        }

        binding.etPassword.apply {
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    if (binding.btnContinue.isEnabled) {
                        viewModel.sendNewPassword(text.toString())
                    }
                    true
                } else {
                    false
                }
            }
        }

        binding.btnContinue.enableWithAplhaWhen(binding.etPassword) { it.isNotEmpty() && it.length >= 8 }

        disposables.add(binding.btnContinue.ninjaTap {
            viewModel.sendNewPassword(binding.etPassword.text.toString())
        })
    }

    private fun setupViewModels() {
        viewModel.state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { state ->
                    when (state) {

                        is NewPasswordState.GetEmail -> {
                            binding.tvEmail.apply {
                                text = state.email
                            }
                        }

                        is NewPasswordState.Success -> {
                            toast("Password successfully changed!")
                            LoginActivity.openActivity(this, state.email)
                            finishAffinity()
                        }

                        is NewPasswordState.Error -> {
                            toast(state.throwable.getThrowableError())
                        }

                        is NewPasswordState.ShowProgressLoading -> {
                            toast("Sending request")
                        }

                        is NewPasswordState.HideProgressLoading -> {
                        }
                    }
                },
                onError = {
                    Timber.e(it)
                }
            ).apply { disposables.add(this) }
    }
}
