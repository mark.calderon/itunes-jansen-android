package com.appetiser.itunesjansen.features.track

import com.appetiser.module.domain.models.track.Track

sealed class TrackListState {

    data class TracksSongs(val tracks: List<Track>) : TrackListState()

    data class TracksFilms(val tracks: List<Track>) : TrackListState()

    data class TracksTvShows(val tracks: List<Track>) : TrackListState()

    data class TracksAudiobooks(val tracks: List<Track>) : TrackListState()

    data class Error(val throwable: Throwable) : TrackListState()

    data class LastVisitTime(val visitTime: String) : TrackListState()

    object ShowProgressLoading : TrackListState()

    object HideProgressLoading : TrackListState()
}
