package com.appetiser.itunesjansen.features.track.detail

import com.appetiser.module.domain.models.track.Track

sealed class TrackDetailState {

    data class CurrentTrack(val track: Track) : TrackDetailState()

    data class Error(val throwable: Throwable) : TrackDetailState()

    object ShowProgressLoading : TrackDetailState()

    object HideProgressLoading : TrackDetailState()
}
