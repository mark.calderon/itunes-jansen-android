package com.appetiser.itunesjansen.features.auth.register.createpassword

import android.os.Bundle
import com.appetiser.itunesjansen.base.BaseViewModel
import com.appetiser.itunesjansen.utils.PASSWORD_MIN_LENGTH
import com.appetiser.module.data.features.auth.AuthRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class CreatePasswordViewModel @Inject constructor(
    private val authRepository: AuthRepository
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<CreatePasswordState>()
    }

    val state: Observable<CreatePasswordState> = _state

    private lateinit var email: String
    private lateinit var phone: String

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun setEmailOrPhone(email: String, phone: String) {
        this.email = email
        this.phone = phone

        val username = if (email.isNotEmpty()) {
            email
        } else {
            phone
        }

        _state
            .onNext(
                CreatePasswordState
                    .FillUsername(username)
            )
    }

    fun savePassword(
        password: String
    ) {
        if (!validatePassword(password)) return

        if (phone.isNotEmpty()) {
            handlePhoneNumber(
                password,
                phone
            )
        } else {
            _state
                .onNext(
                    CreatePasswordState.StartRegisterScreen(
                        password,
                        email
                    )
                )
        }
    }

    /**
     * When user is coming from phone number screen.
     * Call register and send verification code endpoints
     */
    private fun handlePhoneNumber(password: String, phone: String) {
        authRepository
            .register(
                phone = phone,
                password = password,
                confirmPassword = password,
                firstName = "",
                lastName = ""
            )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = {
                    _state
                        .onNext(
                            CreatePasswordState.StartVerificationCodeScreen(
                                phone
                            )
                        )
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    fun onPasswordTextChange(password: String) {
        if (validatePasswordNotEmpty(password)) {
            _state
                .onNext(
                    CreatePasswordState.EnableButton
                )
        } else {
            _state
                .onNext(
                    CreatePasswordState.DisableButton
                )
        }
    }

    private fun validatePassword(password: String): Boolean {
        if (!validatePasswordNotEmpty(password)) return false

        if (password.length < PASSWORD_MIN_LENGTH) {
            _state
                .onNext(
                    CreatePasswordState.PasswordBelowMinLength
                )
            return false
        }

        return true
    }

    private fun validatePasswordNotEmpty(password: String): Boolean {
        return password.isNotEmpty()
    }
}
