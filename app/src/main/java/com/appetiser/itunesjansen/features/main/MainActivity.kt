package com.appetiser.itunesjansen.features.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.appetiser.itunesjansen.R
import com.appetiser.itunesjansen.base.BaseViewModelActivity
import com.appetiser.itunesjansen.databinding.ActivityMainBinding
import com.appetiser.itunesjansen.features.auth.landing.LandingActivity
import com.appetiser.itunesjansen.features.notification.NotificationFragment
import com.appetiser.module.auth_modern.FacebookLoginManager
import com.appetiser.module.auth_modern.GoogleSignInManager
import com.appetiser.module.common.toast
import com.appetiser.module.common.widget.ScrollableViewPager
import com.google.android.material.bottomnavigation.BottomNavigationView
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class MainActivity : BaseViewModelActivity<ActivityMainBinding, MainViewModel>() {
    companion object {
        fun openActivity(context: Context) {
            context.startActivity(
                Intent(
                    context,
                    MainActivity::class.java
                )
            )
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupViewPager()
        setUpViewModelsObserver()
        setupNavigationView()
    }

    private fun setupNavigationView() {
        binding.bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.page_1 -> {
                setCurrentPosition(0)
            }
            R.id.page_2 -> {
                setCurrentPosition(1)
            }
            R.id.page_4 -> {
                setCurrentPosition(3)
            }
            R.id.page_5 -> {
                setCurrentPosition(4)
            }
            else -> {
            }
        }
        true
    }

    private fun setCurrentPosition(position: Int) {
        binding.viewpager.setCurrentItem(position, false)
    }

    private fun setupViewPager() {
        val listOfFragments: MutableList<Fragment> = mutableListOf()
        listOfFragments.add(DummyFragment.newInstance())
        listOfFragments.add(DummyFragment.newInstance())
        listOfFragments.add(DummyFragment.newInstance())
        listOfFragments.add(NotificationFragment.newInstance())
        listOfFragments.add(DummyFragment.newInstance())

        val pagerAdapter = ScrollableViewPager.PageAdapter(supportFragmentManager, listOfFragments)
        binding.viewpager.adapter = pagerAdapter
    }

    private fun setUpViewModelsObserver() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { state ->
                    handleState(state)
                },
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }

    private fun handleState(state: MainState?) {
        when (state) {
            MainState.LogoutSocialLogins -> {
                logoutSocialLogins()
            }
            MainState.LogoutSuccess -> {
                LandingActivity.openActivity(this)
                finishAffinity()
            }
            MainState.ShowProgressLoading -> {
                toast(getString(R.string.sending_request))
            }
        }
    }

    private fun logoutSocialLogins() {
        // Sign out google if it's signed in.
        if (GoogleSignInManager.isSignedIn(this)) {
            GoogleSignInManager(this)
                .run {
                    signOut()
                }
        }

        // Logout facebook if it's logged in.
        if (FacebookLoginManager.isLoggedIn()) {
            FacebookLoginManager.logout()
        }
    }
}
