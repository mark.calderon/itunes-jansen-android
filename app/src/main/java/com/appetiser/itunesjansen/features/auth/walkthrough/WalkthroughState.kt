package com.appetiser.itunesjansen.features.auth.walkthrough

sealed class WalkthroughState {
    class UpdatePageIndicator(
        val previousPage: Int,
        val currentPage: Int
    ) : WalkthroughState()

    class ShowStep4Buttons(
        val previousPage: Int,
        val currentPage: Int
    ) : WalkthroughState()

    class HideStep4Buttons(
        val previousPage: Int,
        val currentPage: Int
    ) : WalkthroughState()

    /**
     * User has logged in and has filled in onboarding details.
     */
    object UserIsLoggedIn : WalkthroughState()

    /**
     * User has logged in but not filled in onboarding details.
     */
    object UserIsLoggedInButNotOnboarded : WalkthroughState()
}
