package com.appetiser.itunesjansen.features.notification

import android.annotation.SuppressLint
import android.content.Context
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import com.appetiser.itunesjansen.R
import com.appetiser.itunesjansen.base.BaseListAdapter
import com.appetiser.itunesjansen.databinding.ItemNotificationBinding
import com.appetiser.itunesjansen.databinding.ItemNotificationHeaderBinding
import com.appetiser.itunesjansen.databinding.ItemNotificationImageBinding
import com.appetiser.itunesjansen.ext.loadAvatarUrl
import com.appetiser.module.common.widget.CustomTypefaceSpan

class NotificationAdapter(val context: Context) : BaseListAdapter<NotificationViewTypeItem, BaseListAdapter.BaseViewViewHolder<NotificationViewTypeItem>>(NotificationDiffCallback()) {

    companion object {
        const val ITEM_TITLE = 0
        const val ITEM_NORMAL = 1
        const val ITEM_IMAGE = 2
    }

    val regularTypeface = ResourcesCompat.getFont(context, R.font.inter_regular)!!
    val boldTypeface = ResourcesCompat.getFont(context, R.font.inter_bold)!!

    override fun getItemViewType(position: Int): Int {
        return when (items[position].type) {
            NotificationType.HEADER -> ITEM_TITLE
            NotificationType.NORMAL -> ITEM_NORMAL
            NotificationType.IMAGE -> ITEM_IMAGE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewViewHolder<NotificationViewTypeItem> {
        return when (viewType) {
            ITEM_NORMAL -> createNotificationViewHolder(parent)
            ITEM_IMAGE -> createNotificationImageViewHolder(parent)
            else -> createNotificationTitleViewHolder(parent)
        }
    }

    override fun onBindViewHolder(holder: BaseViewViewHolder<NotificationViewTypeItem>, position: Int) {
        holder.bind(items[position])
    }

    private fun createNotificationTitleViewHolder(parent: ViewGroup): NotificationTitleItemViewHolder {
        val view =
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_notification_header, parent, false)

        val binding = ItemNotificationHeaderBinding.bind(view)

        return NotificationTitleItemViewHolder(binding)
    }

    private fun createNotificationViewHolder(parent: ViewGroup): NotificationItemViewHolder {
        val view =
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_notification, parent, false)

        val binding = ItemNotificationBinding.bind(view)

        return NotificationItemViewHolder(binding)
    }

    private fun createNotificationImageViewHolder(parent: ViewGroup): NotificationWithImageItemViewHolder {
        val view =
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_notification_image, parent, false)

        val binding = ItemNotificationImageBinding.bind(view)

        return NotificationWithImageItemViewHolder(binding)
    }

    inner class NotificationTitleItemViewHolder(binding: ItemNotificationHeaderBinding) : BaseViewViewHolder<NotificationViewTypeItem>(binding)

    inner class NotificationItemViewHolder(override val binding: ItemNotificationBinding) : BaseViewViewHolder<NotificationViewTypeItem>(binding) {

        @SuppressLint("SetTextI18n")
        override fun bind(item: NotificationViewTypeItem) {

            binding.name.movementMethod = LinkMovementMethod.getInstance()
            val data = "${item.notification?.username} ${item.notification?.message} \n${item.notification?.time}"

            val ssb = SpannableStringBuilder(data)

            // user message
            ssb.setSpan(
                CustomTypefaceSpan(regularTypeface),
                item.usernameLength + 1,
                item.usernameLength + item.messageLength + 1,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE
            )

            ssb.setSpan(
                RelativeSizeSpan(0.9f),
                item.usernameLength + 1,
                item.usernameLength + item.messageLength + 1,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE)

            // user time
            ssb.setSpan(
                CustomTypefaceSpan(regularTypeface),
                item.usernameLength + item.messageLength + 2,
                item.usernameLength + item.messageLength + item.timeLength + 3,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE)

            ssb.setSpan(
                RelativeSizeSpan(0.7f),
                item.usernameLength + item.messageLength + 2,
                item.usernameLength + item.messageLength + item.timeLength + 3,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE)

            binding.name.text = ssb
            binding.avatar.loadAvatarUrl(item.notification?.avatarUrl)
        }
    }

    inner class NotificationWithImageItemViewHolder(override val binding: ItemNotificationImageBinding) : BaseViewViewHolder<NotificationViewTypeItem>(binding) {
        override fun bind(item: NotificationViewTypeItem) {

            binding.name.movementMethod = LinkMovementMethod.getInstance()
            val data = "${item.notification?.username} ${item.notification?.message} \n${item.notification?.time}"

            val ssb = SpannableStringBuilder(data)

            // user message
            ssb.setSpan(
                CustomTypefaceSpan(regularTypeface),
                item.usernameLength + 1,
                item.usernameLength + item.messageLength + 1,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE
            )

            ssb.setSpan(
                RelativeSizeSpan(0.9f),
                item.usernameLength + 1,
                item.usernameLength + item.messageLength + 1,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE)

            // user time
            ssb.setSpan(
                CustomTypefaceSpan(regularTypeface),
                item.usernameLength + item.messageLength + 2,
                item.usernameLength + item.messageLength + item.timeLength + 3,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE)

            ssb.setSpan(
                RelativeSizeSpan(0.7f),
                item.usernameLength + item.messageLength + 2,
                item.usernameLength + item.messageLength + item.timeLength + 3,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE)

            binding.name.text = ssb
            binding.avatar.loadAvatarUrl(item.notification?.avatarUrl)
            binding.image.loadAvatarUrl(item.notification?.imageUrl)
        }
    }
}
