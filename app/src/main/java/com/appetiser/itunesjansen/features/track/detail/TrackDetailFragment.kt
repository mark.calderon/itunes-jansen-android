package com.appetiser.itunesjansen.features.track.detail

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.appetiser.itunesjansen.R
import com.appetiser.itunesjansen.base.BaseViewModelFragment
import com.appetiser.itunesjansen.databinding.FragmentTrackDetailBinding
import com.appetiser.itunesjansen.ext.getThrowableError
import com.appetiser.itunesjansen.utils.BlurTransformation
import com.appetiser.module.common.toast
import com.appetiser.module.domain.models.track.Track
import com.bumptech.glide.Glide
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class TrackDetailFragment : BaseViewModelFragment<FragmentTrackDetailBinding, TrackDetailViewModel>() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewModel()
        setupViews()
    }

    private fun setupViews() {
        binding.apply {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
            toolbar.setNavigationOnClickListener {
                findNavController().popBackStack()
            } }
    }

    private fun setupViewModel() {
        viewModel
            .state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }

    private fun handleState(state: TrackDetailState) {
        when (state) {
            is TrackDetailState.CurrentTrack -> setTrackData(state.track)
            is TrackDetailState.Error -> activity?.toast(state.throwable.getThrowableError())
            TrackDetailState.ShowProgressLoading -> Timber.d("Do Nothing")
            TrackDetailState.HideProgressLoading -> Timber.d("Do Nothing")
        }
    }

    private fun setTrackData(track: Track) {
        binding.item = track

        Glide.with(requireContext())
            .load(track.artworkUrl100)
            .dontAnimate()
            .fitCenter()
            .placeholder(R.drawable.img_placeholder)
            .error(R.drawable.img_placeholder)
            .into(binding.imageViewThumbnail)

        Glide.with(requireContext())
            .load(track.artworkUrl100)
            .dontAnimate()
            .transform(BlurTransformation(requireContext()))
            .into(binding.imageViewBlur)
    }

    override fun getLayoutId(): Int = R.layout.fragment_track_detail
}
