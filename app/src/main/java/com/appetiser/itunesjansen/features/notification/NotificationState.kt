package com.appetiser.itunesjansen.features.notification

sealed class NotificationState {

    data class FetchNotificationItems(val items: List<NotificationViewTypeItem>) : NotificationState()
}
