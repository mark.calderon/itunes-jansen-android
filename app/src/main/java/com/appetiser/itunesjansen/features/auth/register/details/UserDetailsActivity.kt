package com.appetiser.itunesjansen.features.auth.register.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import com.appetiser.auth_modern.databinding.ActivityInputNameBinding
import com.appetiser.itunesjansen.R
import com.appetiser.itunesjansen.base.BaseViewModelActivity
import com.appetiser.itunesjansen.ext.disabledWithAlpha
import com.appetiser.itunesjansen.ext.enabledWithAlpha
import com.appetiser.itunesjansen.ext.getThrowableError
import com.appetiser.itunesjansen.features.auth.register.profile.UploadProfilePhotoActivity
import com.appetiser.module.common.*
import com.jakewharton.rxbinding3.widget.textChangeEvents
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class UserDetailsActivity : BaseViewModelActivity<ActivityInputNameBinding, UserDetailsViewModel>() {

    companion object {
        fun openActivity(context: Context) {
            val intent = Intent(context, UserDetailsActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_input_name

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupToolbar()
        setUpViews()
        observeInputViews()
        setupViewModel()
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }

    private fun setUpViews() {
        binding.etName.apply {
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    if (binding.btnContinue.isEnabled) {
                        sendUserDetails()
                    }
                    true
                } else {
                    false
                }
            }
        }

        disposables.add(binding.btnContinue.ninjaTap {
            sendUserDetails()
        })
    }

    private fun sendUserDetails() {
        viewModel.sendUserDetails(binding.etName.text.toString())
    }

    private fun observeInputViews() {
        binding.etName.capitalizeFirstLetter().addTo(disposables)

        val firstNameObservable = binding.etName.textChangeEvents()
            .skipInitialValue()
            .map { it.text }
            .map { it.isNotEmpty() }

        disposables.add(
            firstNameObservable
                .debounce(NINJA_TAP_THROTTLE_TIME, TimeUnit.MILLISECONDS)
                .observeOn(scheduler.ui())
                .subscribeBy(
                    onNext = {
                        if (it) {
                            binding.btnContinue.enabledWithAlpha()
                        } else {
                            binding.btnContinue.disabledWithAlpha()
                        }
                    },
                    onError = {
                        Timber.e("Error $it")
                    }
                )
        )
    }

    private fun setupViewModel() {
        viewModel.state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { state ->
                    when (state) {

                        is UserDetailsState.UserDetailsUpdated -> {
                            UploadProfilePhotoActivity.openActivity(this)
                            finish()
                        }

                        is UserDetailsState.Error -> {
                            toast(state.throwable.getThrowableError())
                        }
                        is UserDetailsState.ShowProgressLoading -> {
                            toast("Sending request")
                        }
                    }
                },
                onError = {
                    Timber.e(it)
                }
            ).apply { disposables.add(this) }
    }
}
