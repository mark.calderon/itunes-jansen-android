package com.appetiser.itunesjansen.features.track

import android.os.Bundle
import com.appetiser.itunesjansen.base.BaseViewModel
import com.appetiser.itunesjansen.ext.toReadableString
import com.appetiser.module.data.features.track.TrackRepository
import com.appetiser.module.data.features.visit.VisitTimeRepository
import com.appetiser.module.domain.enums.DateTimeFormat
import com.appetiser.module.domain.enums.Kind
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import java.util.*
import javax.inject.Inject

class TrackListViewModel @Inject constructor(
    private val trackRepository: TrackRepository,
    private val visitTimeRepository: VisitTimeRepository
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<TrackListState>()
    }

    val state: Observable<TrackListState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        getLastVisitTime()
        getAllTracks()
    }

    private fun getLastVisitTime() {
        Observable.just(visitTimeRepository.getVisitTime())
            .take(1)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe {
                _state.onNext(TrackListState.LastVisitTime(it))
            }
            .apply {
                disposables.add(this)
            }
    }

    fun getAllTracks() {
        trackRepository
            .getTracks()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(TrackListState.ShowProgressLoading)
            }
            .doOnError {
                _state.onNext(TrackListState.HideProgressLoading)
            }
            .subscribeBy(
                onNext = {
                    _state.onNext(TrackListState.HideProgressLoading)
                    _state.onNext(TrackListState.TracksFilms(it.filter { track -> track.kind == Kind.FILM.value }))
                    _state.onNext(TrackListState.TracksSongs(it.filter { track -> track.kind == Kind.SONG.value }))
                    _state.onNext(TrackListState.TracksTvShows(it.filter { track -> track.kind == Kind.TV_SHOW.value }))
                    _state.onNext(TrackListState.TracksAudiobooks(it.filter { track -> track.wrapperType == Kind.AUDIO_BOOK.value }))
                },
                onError = {
                    _state.onNext(TrackListState.Error(it))
                }
            )
            .apply { disposables.add(this) }
    }

    override fun onCleared() {
        disposables.clear()
        visitTimeRepository
            .saveVisitTime(Date().toReadableString(DateTimeFormat.DATE_TIME) ?: "")
    }
}
