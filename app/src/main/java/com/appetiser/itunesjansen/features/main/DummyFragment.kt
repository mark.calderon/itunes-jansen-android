package com.appetiser.itunesjansen.features.main

import com.appetiser.itunesjansen.R
import com.appetiser.itunesjansen.base.BaseFragment
import com.appetiser.itunesjansen.databinding.FragmentDummyBinding

class DummyFragment : BaseFragment<FragmentDummyBinding>() {
    companion object {
        fun newInstance(): DummyFragment {
            return DummyFragment()
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_dummy
}
