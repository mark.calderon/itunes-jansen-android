package com.appetiser.itunesjansen.features.auth.register.profile

import android.os.Bundle
import com.appetiser.itunesjansen.base.BaseViewModel
import com.appetiser.module.data.features.user.UserRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class UploadPhotoViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    private val _state by lazy {
        PublishSubject.create<UploadPhotoState>()
    }

    val state: Observable<UploadPhotoState> = _state

    fun uploadPhoto(filePath: String) {
        userRepository
            .uploadPhoto(filePath)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(UploadPhotoState.ShowProgressLoading)
            }
            .doOnComplete {
                _state.onNext(UploadPhotoState.HideProgressLoading)
            }
            .subscribeBy(
                onComplete = {
                    _state.onNext(UploadPhotoState.SuccessUploadPhoto)
                },
                onError = {
                    _state.onNext(UploadPhotoState.ErrorUploadPhoto(it))
                }
            )
            .apply { disposables.add(this) }
    }
}
