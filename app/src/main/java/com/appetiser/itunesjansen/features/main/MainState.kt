package com.appetiser.itunesjansen.features.main

sealed class MainState {

    object LogoutSuccess : MainState()

    object LogoutSocialLogins : MainState()

    object ShowProgressLoading : MainState()

    object HideProgressLoading : MainState()
}
