package com.appetiser.itunesjansen.features.notification

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.itunesjansen.base.BaseViewModel
import com.appetiser.module.domain.models.notification.Notification
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class NotificationViewModel @Inject constructor() : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<NotificationState>()
    }

    val state: Observable<NotificationState> = _state

    private val _loadingVisibility by lazy {
        MutableLiveData<Boolean>()
    }

    val loadingVisibility: LiveData<Boolean> = _loadingVisibility

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        Observable.just(dummyNotificationItems())
            .subscribeOn(schedulers.ui())
            .doOnSubscribe {
                _loadingVisibility.value = true
            }
            .delay(3000, TimeUnit.MILLISECONDS)
            .observeOn(schedulers.ui())
            .doOnComplete {
                _loadingVisibility.value = false
            }
            .subscribeBy(
                onNext = {
                    _state.onNext(NotificationState.FetchNotificationItems(it))
                }
            )
            .addTo(disposables)
    }

    private fun dummyNotificationItems(): MutableList<NotificationViewTypeItem> {
        val items = mutableListOf<NotificationViewTypeItem>()
        items.add(NotificationViewTypeItem(NotificationType.HEADER, null, "Today"))
        items.add(NotificationViewTypeItem(NotificationType.NORMAL, Notification(
            id = "1",
            avatarUrl = "https://gravatar.com/avatar/689ad07c81a8bf7962acf964cc3a16ee?s=200&d=robohash&r=x",
            username = "amylee",
            message = "started following you",
            time = "2s",
            imageUrl = "https://gravatar.com/avatar/689ad07c81a8bf7962acf964cc3a16ee?s=200&d=robohash&r=x"
        ), null))
        items.add(NotificationViewTypeItem(NotificationType.IMAGE, Notification(
            id = "2",
            avatarUrl = "https://gravatar.com/avatar/689ad07c81a8bf7962acf964cc3a16ee?s=200&d=robohash&r=x",
            username = "amylee",
            message = "liked your post",
            time = "1m",
            imageUrl = "https://gravatar.com/avatar/689ad07c81a8bf7962acf964cc3a16ee?s=200&d=robohash&r=x"
        ), null))
        items.add(NotificationViewTypeItem(NotificationType.IMAGE, Notification(
            id = "3",
            avatarUrl = "https://gravatar.com/avatar/689ad07c81a8bf7962acf964cc3a16ee?s=200&d=robohash&r=x",
            username = "christopher_bez",
            message = "commented on your post",
            time = "5m",
            imageUrl = "https://gravatar.com/avatar/689ad07c81a8bf7962acf964cc3a16ee?s=200&d=robohash&r=x"
        ), null))
        items.add(NotificationViewTypeItem(NotificationType.HEADER, null, "Last Week"))
        items.add(NotificationViewTypeItem(NotificationType.IMAGE, Notification(
            id = "4",
            avatarUrl = "https://gravatar.com/avatar/689ad07c81a8bf7962acf964cc3a16ee?s=200&d=robohash&r=x",
            username = "72 users",
            message = "liked your post",
            time = "2d",
            imageUrl = "https://gravatar.com/avatar/689ad07c81a8bf7962acf964cc3a16ee?s=200&d=robohash&r=x"
        ), null))
        items.add(NotificationViewTypeItem(NotificationType.NORMAL, Notification(
            id = "4",
            avatarUrl = "https://gravatar.com/avatar/689ad07c81a8bf7962acf964cc3a16ee?s=200&d=robohash&r=x",
            username = "5 users",
            message = "started following you",
            time = "3d",
            imageUrl = "https://gravatar.com/avatar/689ad07c81a8bf7962acf964cc3a16ee?s=200&d=robohash&r=x"
        ), null))
        items.add(NotificationViewTypeItem(NotificationType.IMAGE, Notification(
            id = "4",
            avatarUrl = "https://gravatar.com/avatar/689ad07c81a8bf7962acf964cc3a16ee?s=200&d=robohash&r=x",
            username = "12 users",
            message = "commented on your post",
            time = "3d",
            imageUrl = "https://gravatar.com/avatar/689ad07c81a8bf7962acf964cc3a16ee?s=200&d=robohash&r=x"
        ), null))
        return items
    }
}
