package com.appetiser.itunesjansen.features.track.detail

import android.os.Bundle
import com.appetiser.itunesjansen.base.BaseViewModel
import com.appetiser.module.data.features.track.TrackRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class TrackDetailViewModel @Inject constructor(
    private val trackRepository: TrackRepository
) : BaseViewModel() {

    companion object {
        const val ARG_TRACK_ID = "id"
    }

    private val _state by lazy {
        PublishSubject.create<TrackDetailState>()
    }

    val state: Observable<TrackDetailState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        val id: Int = bundle?.getInt(ARG_TRACK_ID, -1) ?: -1
        getTrack(id)
    }

    private fun getTrack(id: Int) = trackRepository
        .getTrack(id)
        .subscribeOn(schedulers.io())
        .observeOn(schedulers.ui())
        .doOnSubscribe {
            _state.onNext(TrackDetailState.ShowProgressLoading)
        }
        .doOnError {
            _state.onNext(TrackDetailState.HideProgressLoading)
        }
        .subscribeBy(
            onSuccess = {
                _state.onNext(TrackDetailState.CurrentTrack(it))
            },
            onError = {
                _state.onNext(TrackDetailState.Error(it))
            }
        )
        .apply { disposables.add(this) }
}
