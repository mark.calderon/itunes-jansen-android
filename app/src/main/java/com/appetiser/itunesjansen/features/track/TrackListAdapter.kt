package com.appetiser.itunesjansen.features.track

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.appetiser.itunesjansen.R
import com.appetiser.itunesjansen.base.BaseListAdapter
import com.appetiser.itunesjansen.databinding.ItemTrackBinding
import com.appetiser.itunesjansen.utils.BlurTransformation
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.domain.models.track.Track
import com.bumptech.glide.Glide

class TrackListAdapter(
    private val onItemClick: (Track) -> Unit
) : BaseListAdapter<Track, BaseListAdapter.BaseViewViewHolder<Track>>(TrackDiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewViewHolder<Track> {
        val view =
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_track, parent, false)

        val binding = ItemTrackBinding.bind(view)

        return TrackItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewViewHolder<Track>, position: Int) {

        val item = items[position]
        val binding = holder.binding as ItemTrackBinding

        holder.apply {
            bind(item)
            itemView.ninjaTap {
                onItemClick.invoke(item)
            }
        }

        binding.apply {
            Glide.with(holder.itemView.context)
                .load(item.artworkUrl100)
                .dontAnimate()
                .fitCenter()
                .placeholder(R.drawable.img_placeholder)
                .error(R.drawable.img_placeholder)
                .into(imageView)

            Glide.with(holder.itemView.context)
                .load(item.artworkUrl100)
                .dontAnimate()
                .transform(BlurTransformation(holder.itemView.context))
                .into(imageViewBlur)
        }
    }

    inner class TrackItemViewHolder(binding: ItemTrackBinding) : BaseViewViewHolder<Track>(binding)

    object TrackDiffCallback : DiffUtil.ItemCallback<Track>() {
        override fun areItemsTheSame(
            oldItem: Track,
            newItem: Track
        ): Boolean = oldItem.collectionId == newItem.collectionId

        override fun areContentsTheSame(
            oldItem: Track,
            newItem: Track
        ): Boolean = oldItem == newItem
    }
}
