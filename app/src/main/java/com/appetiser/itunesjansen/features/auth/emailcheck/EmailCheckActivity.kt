package com.appetiser.itunesjansen.features.auth.emailcheck

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.inputmethod.EditorInfo
import com.appetiser.auth_modern.databinding.ActivityOnboardEmailBinding
import com.appetiser.itunesjansen.R
import com.appetiser.itunesjansen.base.BaseViewModelActivity
import com.appetiser.itunesjansen.ext.enableWithAplhaWhen
import com.appetiser.itunesjansen.ext.getThrowableError
import com.appetiser.itunesjansen.features.auth.login.LoginActivity
import com.appetiser.itunesjansen.features.auth.register.createpassword.CreatePasswordActivity
import com.appetiser.module.common.isEmailValid
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.toast
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class EmailCheckActivity : BaseViewModelActivity<ActivityOnboardEmailBinding, EmailCheckViewModel>() {

    companion object {
        fun openActivity(context: Context) {
            val intent = Intent(context, EmailCheckActivity::class.java)
            context.startActivity(intent)
        }

        private const val EMAIL = "email"
    }

    override fun canBack(): Boolean {
        return true
    }

    override fun getLayoutId(): Int = R.layout.activity_onboard_email

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupToolbar()
        setupViews(savedInstanceState)
        setupViewModel()
    }

    private fun setupViewModel() {
        viewModel
            .state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { state ->
                    when (state) {
                        is EmailCheckState.EmailExists -> {
                            LoginActivity.openActivity(this, state.email)
                        }

                        is EmailCheckState.EmailDoesNotExist -> {
                            CreatePasswordActivity
                                .openActivity(
                                    this,
                                    email = state.email
                                )
                        }

                        is EmailCheckState.ShowProgressLoading -> {
                            toast("Sending request")
                        }

                        is EmailCheckState.HideProgressLoading -> {
                        }

                        is EmailCheckState.Error -> {
                            // Show error message
                            toast(state.throwable.getThrowableError())
                        }
                    }
                },
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }

    private fun setupViews(savedInstanceState: Bundle?) {
        binding.etEmail.apply {
            setText(savedInstanceState?.getString(EMAIL))
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    val email = binding.etEmail.text.toString()

                    when {
                        email.isEmpty() -> toast(getString(R.string.email_must_not_be_empty))
                        !email.isEmailValid() -> toast(getString(R.string.invalid_email))
                        else -> viewModel.checkEmail(email)
                    }
                    true
                } else {
                    false
                }
            }
        }

        binding
            .btnContinue
            .enableWithAplhaWhen(binding.etEmail) {
                Patterns.EMAIL_ADDRESS.matcher(it).matches()
            }

        binding
            .btnContinue
            .ninjaTap {
                viewModel
                    .checkEmail(
                        binding.etEmail.text.toString()
                    )
            }
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(EMAIL, binding.etEmail.text.toString())
    }
}
