package com.appetiser.itunesjansen

import com.appetiser.module.domain.models.Session
import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.User

object Stubs {

    val USER_LOGGED_IN = User(
        id = "1231232132123",
        fullName = "Jose Mari Chan",
        firstName = "Jose Mari",
        lastName = "Chan",
        email = "josemarichan@gmail.com",
        avatarPermanentThumbUrl = "http://www.google.com/",
        avatarPermanentUrl = "http://www.google.com/",
        phoneNumber = "+639435643214",
        emailVerified = true,
        phoneNumberVerified = true,
        verified = true
    )

    val USER_SESSION_LOGGED_IN_NOT_ONBOARDED = User(
        id = "1231232132123",
        fullName = "",
        firstName = "",
        lastName = "",
        email = "",
        avatarPermanentThumbUrl = "/",
        avatarPermanentUrl = "",
        phoneNumber = "+639435643214",
        emailVerified = true,
        phoneNumberVerified = true,
        verified = true
    )

    val USER_NOT_VERIFIED = User(
        id = "1111",
        fullName = "Test",
        firstName = "Test",
        lastName = "Test",
        email = "test@test.test",
        avatarPermanentThumbUrl = "http://photo",
        avatarPermanentUrl = "http://photo",
        verified = false,
        emailVerified = false,
        phoneNumber = "09177707257",
        phoneNumberVerified = false
    )

    val VALID_ACCESS_TOKEN = AccessToken(token = "sampleToken")

    val SESSION_LOGGED_IN = Session(USER_LOGGED_IN, VALID_ACCESS_TOKEN)

    val SESSION_NOT_VERIFIED = Session(USER_NOT_VERIFIED, VALID_ACCESS_TOKEN)

    val SESSION_USER_LOGGED_IN_NOT_ONBOARDED = Session(USER_SESSION_LOGGED_IN_NOT_ONBOARDED, VALID_ACCESS_TOKEN)
}
