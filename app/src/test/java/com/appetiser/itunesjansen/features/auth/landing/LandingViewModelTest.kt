package com.appetiser.itunesjansen.features.auth.landing

import com.appetiser.itunesjansen.Stubs
import com.appetiser.itunesjansen.core.TestSchedulerProvider
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.domain.utils.any
import com.appetiser.module.domain.utils.argumentCaptor
import com.appetiser.module.domain.utils.capture
import com.appetiser.module.domain.utils.mock
import com.google.gson.Gson
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import org.mockito.internal.matchers.apachecommons.ReflectionEquals

class LandingViewModelTest {

    private lateinit var sut: LandingViewModel

    private val authRepository: AuthRepository = mock()

    private val testScheduler = TestScheduler()
    private val schedulers = TestSchedulerProvider(testScheduler)

    private val observer: TestObserver<LandingState> = mock()

    private val sessionLoggedIn = Stubs.SESSION_LOGGED_IN
    private val sessionLoggedInButNotOnboarded = Stubs.SESSION_USER_LOGGED_IN_NOT_ONBOARDED

    @Before
    fun setUp() {
        sut = LandingViewModel(authRepository, Gson())
        sut.schedulers = schedulers
        sut.state.subscribe(observer)
    }

    @Test
    fun onFacebookLogin_ShouldReturnLoginSuccess_WhenUserOnboardingDetailsAreFilled() {
        val accessToken = "access_token"
        val expectedLoginType = LandingViewModel.FACEBOOK
        val expectedState1 = LandingState.ShowLoading(expectedLoginType)
        val expectedState2 = LandingState.HideLoading
        val expectedState3 = LandingState.SocialLoginSuccess

        `when`(authRepository.socialLogin(any(), any()))
            .thenReturn(Single.just(sessionLoggedIn))

        sut.onFacebookLogin(accessToken)
        testScheduler.triggerActions()

        val accessTokenProviderCaptor = argumentCaptor<String>()

        accessTokenProviderCaptor
            .run {
                verify(authRepository, times(1))
                    .socialLogin(
                        any(),
                        capture(this)
                    )

                Assert.assertEquals(expectedLoginType, value)
            }

        argumentCaptor<LandingState>()
            .run {
                verify(observer, times(3)).onNext(capture())

                Assert
                    .assertTrue(ReflectionEquals(expectedState1).matches(allValues[0]))
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun onFacebookLogin_ShouldReturnLoginSuccessButNotOnboarded_WhenUserOnboardingDetailsAreNotFilled() {
        val accessToken = "access_token"
        val expectedLoginType = LandingViewModel.FACEBOOK
        val expectedState1 = LandingState.ShowLoading(expectedLoginType)
        val expectedState2 = LandingState.HideLoading
        val expectedState3 = LandingState.SocialLoginSuccessButNotOnboarded

        `when`(authRepository.socialLogin(any(), any()))
            .thenReturn(Single.just(sessionLoggedInButNotOnboarded))

        sut.onFacebookLogin(accessToken)
        testScheduler.triggerActions()

        argumentCaptor<String>()
            .run {
                verify(authRepository, times(1))
                    .socialLogin(
                        any(),
                        capture(this)
                    )

                Assert.assertEquals(expectedLoginType, value)
            }

        argumentCaptor<LandingState>()
            .run {
                verify(observer, times(3)).onNext(capture())

                Assert
                    .assertTrue(ReflectionEquals(expectedState1).matches(allValues[0]))
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun onFacebookLogin_ShouldReturnError_WhenErrorOccurs() {
        val accessToken = "access_token"
        val expectedLoginType = LandingViewModel.FACEBOOK
        val exception = Exception("Mock exception")
        val expectedState1 = LandingState.ShowLoading(expectedLoginType)
        val expectedState2 = LandingState.HideLoading
        val expectedState3 = LandingState.Error(exception)

        `when`(authRepository.socialLogin(any(), any()))
            .thenReturn(Single.error(exception))

        sut.onFacebookLogin(accessToken)
        testScheduler.triggerActions()

        val accessTokenProviderCaptor = argumentCaptor<String>()

        accessTokenProviderCaptor
            .run {
                verify(authRepository, times(1))
                    .socialLogin(
                        any(),
                        capture(this)
                    )

                Assert.assertEquals(expectedLoginType, value)
            }

        argumentCaptor<LandingState>()
            .run {
                verify(observer, times(3)).onNext(capture())

                Assert
                    .assertTrue(ReflectionEquals(expectedState1).matches(allValues[0]))
                Assert.assertEquals(expectedState2, allValues[1])
                Assert
                    .assertTrue(ReflectionEquals(expectedState3).matches(allValues[2]))
            }
    }

    @Test
    fun onGoogleSignIn_ShouldReturnLoginSuccess_WhenUserOnboardingDetailsAreFilled() {
        val accessToken = "access_token"
        val expectedLoginType = LandingViewModel.GOOGLE
        val expectedState1 = LandingState.ShowLoading(expectedLoginType)
        val expectedState2 = LandingState.HideLoading
        val expectedState3 = LandingState.SocialLoginSuccess

        `when`(authRepository.socialLogin(any(), any()))
            .thenReturn(Single.just(sessionLoggedIn))

        sut.onGoogleSignIn(accessToken)
        testScheduler.triggerActions()

        val accessTokenProviderCaptor = argumentCaptor<String>()

        accessTokenProviderCaptor
            .run {
                verify(authRepository, times(1))
                    .socialLogin(
                        any(),
                        capture(this)
                    )

                Assert.assertEquals(expectedLoginType, value)
            }

        argumentCaptor<LandingState>()
            .run {
                verify(observer, times(3)).onNext(capture())

                Assert
                    .assertTrue(ReflectionEquals(expectedState1).matches(allValues[0]))
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun onGoogleSignIn_ShouldReturnLoginSuccessButNotOnboarded_WhenUserOnboardingDetailsAreNotFilled() {
        val accessToken = "access_token"
        val expectedLoginType = LandingViewModel.GOOGLE
        val expectedState1 = LandingState.ShowLoading(expectedLoginType)
        val expectedState2 = LandingState.HideLoading
        val expectedState3 = LandingState.SocialLoginSuccessButNotOnboarded

        `when`(authRepository.socialLogin(any(), any()))
            .thenReturn(Single.just(sessionLoggedInButNotOnboarded))

        sut.onGoogleSignIn(accessToken)
        testScheduler.triggerActions()

        argumentCaptor<String>()
            .run {
                verify(authRepository, times(1))
                    .socialLogin(
                        any(),
                        capture(this)
                    )

                Assert.assertEquals(expectedLoginType, value)
            }

        argumentCaptor<LandingState>()
            .run {
                verify(observer, times(3)).onNext(capture())

                Assert
                    .assertTrue(ReflectionEquals(expectedState1).matches(allValues[0]))
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun onGoogleSignIn_ShouldReturnError_WhenErrorOccurs() {
        val accessToken = "access_token"
        val expectedLoginType = LandingViewModel.GOOGLE
        val exception = Exception("Mock exception")
        val expectedState1 = LandingState.ShowLoading(expectedLoginType)
        val expectedState2 = LandingState.HideLoading
        val expectedState3 = LandingState.Error(exception)

        `when`(authRepository.socialLogin(any(), any()))
            .thenReturn(Single.error(exception))

        sut.onGoogleSignIn(accessToken)
        testScheduler.triggerActions()

        val accessTokenProviderCaptor = argumentCaptor<String>()

        accessTokenProviderCaptor
            .run {
                verify(authRepository, times(1))
                    .socialLogin(
                        any(),
                        capture(this)
                    )

                Assert.assertEquals(expectedLoginType, value)
            }

        argumentCaptor<LandingState>()
            .run {
                verify(observer, times(3)).onNext(capture())

                Assert
                    .assertTrue(ReflectionEquals(expectedState1).matches(allValues[0]))
                Assert.assertEquals(expectedState2, allValues[1])
                Assert
                    .assertTrue(ReflectionEquals(expectedState3).matches(allValues[2]))
            }
    }
}
