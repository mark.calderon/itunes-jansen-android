package com.appetiser.itunesjansen.features.auth.register

import com.appetiser.itunesjansen.Stubs
import com.appetiser.itunesjansen.core.TestSchedulerProvider
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.domain.models.Session
import com.appetiser.module.domain.utils.any
import com.appetiser.module.domain.utils.argumentCaptor
import com.appetiser.module.domain.utils.mock
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.times
import org.mockito.internal.matchers.apachecommons.ReflectionEquals

class RegisterViewModelTest {

    private val authRepository: AuthRepository = mock()

    private val testScheduler = TestScheduler()
    private val schedulers = TestSchedulerProvider(testScheduler)
    private val observer: TestObserver<RegisterState> = mock() as TestObserver<RegisterState>

    private lateinit var subject: RegisterViewModel

    @Before
    fun setUp() {
        subject = RegisterViewModel(authRepository)
        subject.schedulers = schedulers
        subject.state.subscribe(observer)
    }

    @Test
    fun register_ShouldEmitSaveLoginCredentials_WhenResponseIsSuccessful() {
        val userSession = Stubs.USER_NOT_VERIFIED
        val accessToken = Stubs.VALID_ACCESS_TOKEN
        val expectedState0 = RegisterState.ShowProgressLoading
        val expectedState1 = RegisterState.HideProgressLoading
        val expectedState2 = RegisterState.SaveLoginCredentials(userSession)
        val response = Session(userSession, accessToken)

        `when`(authRepository.register(any(), any(), any(), any(), any(), any()))
            .thenReturn(Single.just(response))

        subject.setEmailAndPassword(userSession.email, "12345678")
        subject.register(userSession.phoneNumber)
        testScheduler.triggerActions()

        argumentCaptor<RegisterState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState0, allValues[0])
                Assert.assertEquals(expectedState1, allValues[1])
                Assert
                    .assertTrue(
                        ReflectionEquals(expectedState2).matches(allValues[2])
                    )
            }
    }

    @Test
    fun register_ShouldEmitError_WhenResponseThrowsException() {
        val userSession = Stubs.USER_NOT_VERIFIED
        val errorMessage = "Test error."
        val throwable = Throwable(errorMessage)
        val expectedState0 = RegisterState.ShowProgressLoading
        val expectedState1 = RegisterState.HideProgressLoading
        val expectedState2 = RegisterState.Error(throwable)

        `when`(authRepository.register(any(), any(), any(), any(), any(), any()))
            .thenReturn(Single.error(throwable))

        subject.setEmailAndPassword(userSession.email, "12345678")
        subject.register(userSession.phoneNumber)
        testScheduler.triggerActions()

        argumentCaptor<RegisterState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState0, allValues[0])
                Assert.assertEquals(expectedState1, allValues[1])
                Assert
                    .assertTrue(
                        ReflectionEquals(expectedState2).matches(allValues[2])
                    )
            }
    }
}
