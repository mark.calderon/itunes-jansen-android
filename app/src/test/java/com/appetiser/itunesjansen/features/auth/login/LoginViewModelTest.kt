package com.appetiser.itunesjansen.features.auth.login

import com.appetiser.itunesjansen.Stubs
import com.appetiser.itunesjansen.Stubs.SESSION_LOGGED_IN
import com.appetiser.itunesjansen.Stubs.SESSION_NOT_VERIFIED
import com.appetiser.itunesjansen.core.TestSchedulerProvider
import com.appetiser.module.data.features.auth.AuthRepository
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.*

class LoginViewModelTest {

    private lateinit var loginViewModel: LoginViewModel

    private val testScheduler = TestScheduler()
    private val schedulers = TestSchedulerProvider(testScheduler)

    private val repository = mock(AuthRepository::class.java)
    private val observer = mock(TestObserver::class.java) as TestObserver<LoginState>

    @Before
    fun setup() {
        loginViewModel = LoginViewModel(repository)
        loginViewModel.schedulers = schedulers
        loginViewModel.state.subscribe(observer)
    }

    @Test
    fun `when user login expect user session email verified`() {
        val email = "test@test.test"
        val password = "password"
        val session = SESSION_LOGGED_IN
        val expected = LoginState.LoginSuccess(user = Stubs.USER_LOGGED_IN)

        `when`(repository.login(email, password)).thenReturn(Single.just(session))

        loginViewModel.login(email, password)
        testScheduler.triggerActions()

        ArgumentCaptor.forClass(LoginState.LoginSuccess::class.java).run {
            verify(observer, times(3)).onNext(capture())
            Assert.assertEquals(expected, value)
        }
    }

    @Test
    fun `when user login expect user session not verified`() {
        val email = "test@test.test"
        val password = "password"
        val session = SESSION_NOT_VERIFIED
        val expected =
            LoginState
                .UserNotVerified(
                    session.user,
                    session.user.email,
                    session.user.phoneNumber
                )

        `when`(repository.login(email, password)).thenReturn(Single.just(session))

        loginViewModel.login(email, password)
        testScheduler.triggerActions()

        ArgumentCaptor.forClass(LoginState.UserNotVerified::class.java).run {
            verify(observer, times(3)).onNext(capture())
            Assert.assertEquals(expected, value)
        }
    }

    @Test
    fun `when user login expect error`() {
        val email = "foo@bar.baz"
        val password = "password"

        val error = Throwable("Something went wrong")

        val expected = LoginState.Error(error)

        `when`(repository.login(username = email, password = password)).thenReturn(Single.error(error))

        loginViewModel.login(email, password)
        testScheduler.triggerActions()

        ArgumentCaptor.forClass(LoginState.Error::class.java).run {
            verify(observer, atLeast(2)).onNext(capture())
            Assert.assertEquals(expected, value)
        }
    }
}
