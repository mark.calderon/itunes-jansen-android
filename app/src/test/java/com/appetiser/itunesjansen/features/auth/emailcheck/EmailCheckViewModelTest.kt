package com.appetiser.itunesjansen.features.auth.emailcheck

import com.appetiser.itunesjansen.core.TestSchedulerProvider
import com.appetiser.itunesjansen.core.TestUtils.Companion.buildErrorResponseCheckEmail
import com.appetiser.module.data.features.auth.AuthRepository
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.*
import retrofit2.HttpException

class EmailCheckViewModelTest {

    private lateinit var viewModel: EmailCheckViewModel

    private val testScheduler = TestScheduler()
    private val schedulers = TestSchedulerProvider(testScheduler)

    private val authRepository = mock(AuthRepository::class.java)
    private val observer = mock(TestObserver::class.java) as TestObserver<EmailCheckState>

    @Before
    fun setup() {
        viewModel = EmailCheckViewModel(authRepository)
        viewModel.schedulers = schedulers
        viewModel.state.subscribe(observer)
    }

    @Test
    fun `when user input email check expect email exist`() {
        val email = "foo@bar.baz"

        val exist = true
        val expected = EmailCheckState.EmailExists(email)

        `when`(authRepository.checkUsername(email)).thenReturn(Single.just(exist))

        viewModel.checkEmail(email)
        testScheduler.triggerActions()

        ArgumentCaptor.forClass(EmailCheckState.EmailExists::class.java).run {
            verify(observer, times(3)).onNext(capture())
            Assert.assertEquals(expected, value)
        }
    }

    @Test
    fun `when user input email check and expect email does not exist`() {
        val email = "foo1233123@bar.baz"

        val error = HttpException(buildErrorResponseCheckEmail())
        val expected = EmailCheckState.EmailDoesNotExist(email)

        `when`(authRepository.checkUsername(email)).thenReturn(Single.error(error))

        viewModel.checkEmail(email)
        testScheduler.triggerActions()

        ArgumentCaptor.forClass(EmailCheckState.EmailDoesNotExist::class.java).run {
            verify(observer, times(3)).onNext(capture())
            Assert.assertEquals(expected, value)
        }
    }

    @Test
    fun `when user input email expect error`() {
        val email = "foo@bar.baz"

        val error = Throwable("Something went wrong")
        val expected = EmailCheckState.Error(error)

        `when`(authRepository.checkUsername(email)).thenReturn(Single.error(error))

        viewModel.checkEmail(email)
        testScheduler.triggerActions()

        ArgumentCaptor.forClass(EmailCheckState.Error::class.java).run {
            verify(observer, times(3)).onNext(capture())
            Assert.assertEquals(expected, value)
        }
    }
}
