package com.appetiser.itunesjansen.features.auth.forgotpassword

import com.appetiser.itunesjansen.core.TestSchedulerProvider
import com.appetiser.module.data.features.auth.AuthRepository
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.mockito.Mockito

class ForgotPasswordViewModelTest {

    private lateinit var viewModel: ForgotPasswordViewModel

    private val testScheduler = TestScheduler()
    private val schedulers = TestSchedulerProvider(testScheduler)

    private val repository = Mockito.mock(AuthRepository::class.java)
    private val observer = Mockito.mock(TestObserver::class.java) as TestObserver<ForgotPasswordState>

    @Before
    fun setup() {
        viewModel = ForgotPasswordViewModel(repository)
        viewModel.schedulers = schedulers
        viewModel.state.subscribe(observer)
    }
}
