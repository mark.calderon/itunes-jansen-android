package com.appetiser.itunesjansen.features.auth.upload

import com.appetiser.itunesjansen.core.TestSchedulerProvider
import com.appetiser.itunesjansen.features.auth.register.profile.UploadPhotoState
import com.appetiser.itunesjansen.features.auth.register.profile.UploadPhotoViewModel
import com.appetiser.module.data.features.user.UserRepository
import com.appetiser.module.domain.utils.mock
import io.reactivex.Completable
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.*

class UploadPhotoViewModelTest {

    private lateinit var uploadPhotoViewModel: UploadPhotoViewModel

    private val testScheduler = TestScheduler()
    private val schedulers = TestSchedulerProvider(testScheduler)

    private val repository: UserRepository = mock()
    private val observer: TestObserver<UploadPhotoState> = mock()

    @Before
    fun setup() {
        uploadPhotoViewModel = UploadPhotoViewModel(repository)
        uploadPhotoViewModel.schedulers = schedulers
        uploadPhotoViewModel.state.subscribe(observer)
    }

    @Test
    fun `when user upload a photo expect success`() {

        val filePath = "/mnt/test"
        val expected = UploadPhotoState.SuccessUploadPhoto

        `when`(repository.uploadPhoto(filePath)).thenReturn(Completable.complete())

        uploadPhotoViewModel.uploadPhoto(filePath)
        testScheduler.triggerActions()

        ArgumentCaptor.forClass(UploadPhotoState.SuccessUploadPhoto::class.java).run {
            verify(observer, times(3)).onNext(capture())
            Assert.assertEquals(expected, value)
        }
    }

    @Test
    fun `when user upload a photo expect error`() {
        val filePath = "/mnt/test"

        val error = Throwable("Something went wrong")

        val expected = UploadPhotoState.ErrorUploadPhoto(error)

        `when`(repository.uploadPhoto(filePath)).thenReturn(Completable.error(error))

        uploadPhotoViewModel.uploadPhoto(filePath)
        testScheduler.triggerActions()

        ArgumentCaptor.forClass(UploadPhotoState.ErrorUploadPhoto::class.java).run {
            verify(observer, atLeast(1)).onNext(capture())
            Assert.assertEquals(expected, value)
        }
    }
}
