package com.appetiser.module.local

import android.app.Application
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.local.features.session.SessionLocalSourceImpl
import com.appetiser.module.local.features.token.AccessTokenLocalSource
import com.appetiser.module.local.features.token.AccessTokenLocalSourceImpl
import com.appetiser.module.local.features.tracks.TrackLocalSource
import com.appetiser.module.local.features.tracks.TrackLocalSourceImpl
import com.appetiser.module.local.features.user.UserLocalSource
import com.appetiser.module.local.features.user.UserLocalSourceImpl
import com.appetiser.module.local.features.visit.VisitTimeLocalSource
import com.appetiser.module.local.features.visit.VisitTimeLocalSourceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class StorageModule {

    @Provides
    @Singleton
    fun providesSharedPreferences(application: Application): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(application.applicationContext)
    }

    @Provides
    @Singleton
    fun providesUserLocalSource(appDatabase: AppDatabase): UserLocalSource {
        return UserLocalSourceImpl(appDatabase.userDao())
    }

    @Provides
    @Singleton
    fun providesTrackLocalSource(appDatabase: AppDatabase): TrackLocalSource {
        return TrackLocalSourceImpl(appDatabase.trackDao())
    }

    @Provides
    @Singleton
    fun providesVisitTimeLocalSource(sharedPreferences: SharedPreferences): VisitTimeLocalSource {
        return VisitTimeLocalSourceImpl(sharedPreferences)
    }

    @Provides
    @Singleton
    fun providesAccessTokenLocalSource(
        sharedPreferences: SharedPreferences,
        appDatabase: AppDatabase
    ): AccessTokenLocalSource {
        return AccessTokenLocalSourceImpl(
            sharedPreferences,
            appDatabase.tokenDao()
        )
    }

    @Provides
    @Singleton
    fun providesSessionLocalSource(
        userLocalSource: UserLocalSource,
        accessTokenLocalSource: AccessTokenLocalSource
    ): SessionLocalSource {
        return SessionLocalSourceImpl(
            userLocalSource,
            accessTokenLocalSource
        )
    }
}
