package com.appetiser.module.local.features.visit

import android.content.SharedPreferences
import javax.inject.Inject

class VisitTimeLocalSourceImpl @Inject constructor(
    private val sharedPreferences: SharedPreferences
) : VisitTimeLocalSource {

    companion object {

        private const val PREF_VISIT_TIME = "PREF_VISIT_TIME"
    }

    override fun saveVisitTime(time: String) {
        sharedPreferences
            .edit()
            .apply {
                putString(PREF_VISIT_TIME, time)
                apply()
            }
    }

    override fun getVisitTime(): String {
        return sharedPreferences.getString(PREF_VISIT_TIME, "") ?: ""
    }
}
