package com.appetiser.module.local.features.tracks.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.appetiser.module.domain.models.track.Track

@Entity(tableName = TrackDB.TRACK_TABLE_NAME)
data class TrackDB(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "pk_id")
    val collectionId: Int,
    @ColumnInfo(name = "wrapper_type")
    val wrapperType: String,
    val kind: String? = null,
    @ColumnInfo(name = "artist_id")
    val artistId: Int,
    @ColumnInfo(name = "artist_name")
    val artistName: String,
    @ColumnInfo(name = "track_name")
    val trackName: String? = null,
    @ColumnInfo(name = "collection_name")
    val collectionName: String? = null,
    @ColumnInfo(name = "collection_censored_name")
    val collectionCensoredName: String? = null,
    @ColumnInfo(name = "artist_view_url")
    val artistViewUrl: String? = null,
    @ColumnInfo(name = "collection_view_url")
    val collectionViewUrl: String? = null,
    @ColumnInfo(name = "artwork_url_60")
    val artworkUrl60: String,
    @ColumnInfo(name = "artwork_url_100")
    val artworkUrl100: String,
    @ColumnInfo(name = "collection_price")
    val collectionPrice: Double,
    @ColumnInfo(name = "collection_explicitness")
    val collectionExplicitness: String,
    @ColumnInfo(name = "track_count")
    val trackCount: Int,
    val copyright: String? = null,
    val country: String,
    val currency: String,
    @ColumnInfo(name = "release_date")
    val releaseDate: String,
    @ColumnInfo(name = "primary_genre_name")
    val primaryGenreName: String,
    @ColumnInfo(name = "preview_url")
    val previewUrl: String,
    val description: String? = null,
    @ColumnInfo(name = "long_description")
    val longDescription: String? = null

) {
    companion object {

        const val TRACK_TABLE_NAME = "track"

        fun toDomain(trackDB: TrackDB): Track {
            return with(trackDB) {
                Track(
                    collectionId = collectionId,
                    wrapperType = wrapperType,
                    kind = kind,
                    artistId = artistId,
                    artistName = artistName,
                    trackName = trackName,
                    collectionName = collectionName,
                    collectionCensoredName = collectionCensoredName,
                    artistViewUrl = artistViewUrl,
                    artworkUrl60 = artworkUrl60,
                    artworkUrl100 = artworkUrl100,
                    collectionPrice = collectionPrice,
                    collectionViewUrl = collectionViewUrl,
                    collectionExplicitness = collectionExplicitness,
                    trackCount = trackCount,
                    copyright = copyright,
                    country = country,
                    currency = currency,
                    releaseDate = releaseDate,
                    primaryGenreName = primaryGenreName,
                    previewUrl = previewUrl,
                    description = description,
                    longDescription = longDescription
                )
            }
        }

        fun fromDomain(track: Track): TrackDB {
            return with(track) {
                TrackDB(
                    collectionId = collectionId,
                    wrapperType = wrapperType,
                    kind = kind,
                    artistId = artistId,
                    artistName = artistName,
                    trackName = trackName,
                    collectionName = collectionName,
                    collectionCensoredName = collectionCensoredName,
                    artistViewUrl = artistViewUrl,
                    artworkUrl60 = artworkUrl60,
                    artworkUrl100 = artworkUrl100,
                    collectionPrice = collectionPrice,
                    collectionViewUrl = collectionViewUrl,
                    collectionExplicitness = collectionExplicitness,
                    trackCount = trackCount,
                    copyright = copyright,
                    country = country,
                    currency = currency,
                    releaseDate = releaseDate,
                    primaryGenreName = primaryGenreName,
                    previewUrl = previewUrl,
                    description = description,
                    longDescription = longDescription
                )
            }
        }
    }
}
