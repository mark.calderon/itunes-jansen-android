package com.appetiser.module.local.features.visit

interface VisitTimeLocalSource {

    fun saveVisitTime(time: String)

    fun getVisitTime(): String
}
