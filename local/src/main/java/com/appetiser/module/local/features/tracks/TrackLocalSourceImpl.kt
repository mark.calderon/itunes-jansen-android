package com.appetiser.module.local.features.tracks

import com.appetiser.module.domain.enums.Kind
import com.appetiser.module.domain.models.track.Track
import com.appetiser.module.local.features.tracks.dao.TrackDao
import com.appetiser.module.local.features.tracks.models.TrackDB
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

data class TrackLocalSourceImpl @Inject constructor(
    private val trackDao: TrackDao
) : TrackLocalSource {

    override fun getTracks(): Flowable<List<Track>> = trackDao
        .getAll()
        .map {
            it.map { trackDB ->
                TrackDB.toDomain(trackDB)
            }
        }

    override fun getTracksByKind(kind: Kind): Flowable<List<Track>> = trackDao
        .getTrackByKind(kind.value)
        .map {
            it.map { trackDB ->
                TrackDB.toDomain(trackDB)
            }
        }

    override fun getTrack(id: Int): Single<Track> = trackDao
        .getTrack(id)
        .map {
            TrackDB.toDomain(it)
        }

    override fun deleteAll(): Completable = trackDao.deleteAll()

    override fun saveAll(tracks: List<Track>): Single<List<Track>> {
        val list = tracks.map { TrackDB.fromDomain(it) }
        return trackDao
            .deleteAll()
            .andThen(trackDao.saveAll(*list.toTypedArray()))
            .map {
                tracks
            }
    }
}
