package com.appetiser.module.local.features.token.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.appetiser.module.domain.models.token.AccessToken

@Entity(tableName = AccessTokenDB.TOKEN_TABLE_NAME)
data class AccessTokenDB(
    @PrimaryKey
    var token: String = "",
    var refresh: String = "",
    @ColumnInfo(name = "token_type")
    var tokenType: String = "",
    @ColumnInfo(name = "expires_in")
    var expiresIn: String = ""
) {
    companion object {
        const val TOKEN_TABLE_NAME = "token"

        fun toDomain(accessTokenDB: AccessTokenDB): AccessToken {
            return with(accessTokenDB) {
                AccessToken(
                    token = token,
                    refresh = "",
                    tokenType = tokenType,
                    expiresIn = expiresIn
                )
            }
        }

        fun fromDomain(accessToken: AccessToken): AccessTokenDB {
            return with(accessToken) {
                AccessTokenDB(
                    token = token,
                    refresh = refresh,
                    tokenType = tokenType,
                    expiresIn = expiresIn
                )
            }
        }
    }

    constructor() : this("", "", "", "")

    val bearerToken: String get() = "Bearer $token"
}
