package com.appetiser.module.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.appetiser.module.local.features.token.dao.TokenDao
import com.appetiser.module.local.features.token.models.AccessTokenDB
import com.appetiser.module.local.features.tracks.dao.TrackDao
import com.appetiser.module.local.features.tracks.models.TrackDB
import com.appetiser.module.local.features.user.dao.UserDao
import com.appetiser.module.local.features.user.models.UserDB
import net.sqlcipher.BuildConfig
import net.sqlcipher.database.SQLiteDatabase
import net.sqlcipher.database.SupportFactory

@Database(
    entities = [
        UserDB::class,
        TrackDB::class,
        AccessTokenDB::class
    ],
    version = 2,
    exportSchema = true
)
@TypeConverters
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

    abstract fun trackDao(): TrackDao

    abstract fun tokenDao(): TokenDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context): AppDatabase {
            val dbName = "itunesjansen.db"

            val builder = Room.databaseBuilder(context, AppDatabase::class.java, dbName)
                .fallbackToDestructiveMigration()

            if (!BuildConfig.DEBUG) {
                val passphrase = SQLiteDatabase.getBytes(dbName.toCharArray())
                val factory = SupportFactory(passphrase)
                builder.openHelperFactory(factory)
            }

            return builder.build()
        }
    }
}
