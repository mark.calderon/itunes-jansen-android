package com.appetiser.module.local.features.tracks

import com.appetiser.module.domain.enums.Kind
import com.appetiser.module.domain.models.track.Track
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface TrackLocalSource {

    fun getTracks(): Flowable<List<Track>>

    fun getTracksByKind(kind: Kind): Flowable<List<Track>>

    fun getTrack(id: Int): Single<Track>

    fun deleteAll(): Completable

    fun saveAll(tracks: List<Track>): Single<List<Track>>
}
