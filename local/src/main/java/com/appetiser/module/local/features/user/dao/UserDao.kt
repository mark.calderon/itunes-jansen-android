package com.appetiser.module.local.features.user.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.appetiser.module.local.base.BaseDao
import com.appetiser.module.local.features.user.models.UserDB
import io.reactivex.Completable
import io.reactivex.Single

@Dao
abstract class UserDao : BaseDao<UserDB> {

    @Query("SELECT * FROM ${UserDB.USER_TABLE_NAME} LIMIT 1")
    abstract fun getUserInfo(): Single<UserDB>

    @Query("DELETE FROM ${UserDB.USER_TABLE_NAME}")
    abstract fun logoutUser()

    @Query("UPDATE ${UserDB.USER_TABLE_NAME} SET full_name = :fullName, avatar_permanent_url = :photoUrl WHERE email =:email")
    abstract fun updateUser(fullName: String?, email: String?, photoUrl: String?): Int

    @Query("SELECT * FROM ${UserDB.USER_TABLE_NAME} LIMIT 1")
    abstract fun getUser(): Single<UserDB>

    @Query("DELETE FROM ${UserDB.USER_TABLE_NAME}")
    abstract fun deleteUsers(): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun saveUser(dbUser: UserDB): Single<Long>
}
