package com.appetiser.module.local.features.user.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.appetiser.module.domain.models.user.User

@Entity(tableName = UserDB.USER_TABLE_NAME)
data class UserDB(
    @PrimaryKey(autoGenerate = true)
    var pk_id: Long? = null,
    @ColumnInfo(name = "full_name")
    var fullName: String? = "",
    @ColumnInfo(name = "first_name")
    var firstName: String? = "",
    @ColumnInfo(name = "last_name")
    var lastName: String? = "",
    var email: String? = "",
    @ColumnInfo(name = "avatar_permanent_url")
    var avatarPermanentUrl: String? = "",
    @ColumnInfo(name = "avatar_permanent_thumb_url")
    var avatarPermanentThumbUrl: String? = "",
    @ColumnInfo(name = "phone_number")
    var phoneNumber: String? = "",
    @ColumnInfo(name = "email_verified")
    var emailVerified: Boolean = false,
    @ColumnInfo(name = "phone_number_verified")
    var phoneNumberVerified: Boolean = false,
    var verified: Boolean = false,
    var uid: String = ""
) {

    companion object {
        const val USER_TABLE_NAME = "user"
        const val EMPTY_USER_ID = "empty"

        /**
         * Returns an empty user.
         */
        fun empty(): UserDB {
            return UserDB(
                fullName = EMPTY_USER_ID
            )
        }

        fun fromDomain(user: User): UserDB {
            return with(user) {
                UserDB(
                    fullName = fullName,
                    firstName = firstName,
                    lastName = lastName,
                    email = email,
                    avatarPermanentUrl = avatarPermanentUrl.orEmpty(),
                    avatarPermanentThumbUrl = avatarPermanentThumbUrl.orEmpty(),
                    phoneNumber = phoneNumber,
                    emailVerified = emailVerified,
                    phoneNumberVerified = phoneNumberVerified,
                    verified = verified,
                    uid = id.orEmpty()
                )
            }
        }

        fun toDomain(userDB: UserDB): User {
            return with(userDB) {
                User(
                    phoneNumber = phoneNumber.orEmpty(),
                    emailVerified = emailVerified,
                    phoneNumberVerified = phoneNumberVerified,
                    verified = verified,
                    email = email.orEmpty(),
                    lastName = lastName.orEmpty(),
                    firstName = firstName.orEmpty(),
                    fullName = fullName.orEmpty(),
                    avatarPermanentUrl = avatarPermanentUrl.orEmpty(),
                    avatarPermanentThumbUrl = avatarPermanentThumbUrl.orEmpty(),
                    id = uid
                )
            }
        }
    }

    fun isEmptyUser(): Boolean {
        return fullName == EMPTY_USER_ID
    }

    constructor() : this(
        0, "", "", "", "", "",
        "", "", false, false, false, ""
    )
}
