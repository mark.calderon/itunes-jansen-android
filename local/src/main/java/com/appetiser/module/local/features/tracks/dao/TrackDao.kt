package com.appetiser.module.local.features.tracks.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.appetiser.module.local.base.BaseDao
import com.appetiser.module.local.features.tracks.models.TrackDB
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
abstract class TrackDao : BaseDao<TrackDB> {

    @Query("SELECT * FROM ${TrackDB.TRACK_TABLE_NAME}")
    abstract fun getAll(): Flowable<List<TrackDB>>

    @Query("SELECT * FROM ${TrackDB.TRACK_TABLE_NAME} WHERE pk_id = :id LIMIT 1")
    abstract fun getTrack(id: Int): Single<TrackDB>

    @Query("SELECT * FROM ${TrackDB.TRACK_TABLE_NAME} WHERE kind = :kind")
    abstract fun getTrackByKind(kind: String): Flowable<List<TrackDB>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun saveAll(vararg tracks: TrackDB): Single<List<Long>>

    @Query("DELETE FROM ${TrackDB.TRACK_TABLE_NAME}")
    abstract fun deleteAll(): Completable
}
